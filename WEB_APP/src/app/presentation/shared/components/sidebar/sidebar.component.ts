import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';



/**
 * Componente que se encarga de renderizar la pantalla de login
 */
@Component({
  selector: 'viaje-seguro-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SideBarComponent {
  activeMenu: number = 0
  @Output() onNavigate = new EventEmitter<string>();
  menus: Array<MenuItem> = [
    {
      icon: 'far fa-comment-alt',
      title: 'Dashboard',
      active: true,
      route: 'dashboard'
    },
    {
      icon: 'fas fa-location-arrow',
      title: 'Expedientes',
      active: false,
      route: 'proceedings/resolutor'
    },
    {
      icon: 'fas fa-pencil-alt',
      title: 'Resoluciones',
      active: false,
      route: 'proceedings/resolution'
    }
  ]

  constructor(private router : Router) {}

  menuClicked(position: number){
    this.menus[this.activeMenu].active = false;
    this.activeMenu = position;
    this.menus[this.activeMenu].active = true;
    this.onNavigate.emit(this.menus[position].title);
  }

  ngOnInit() {
    this.clearMenu()
    if (this.router.url.indexOf('dashboard') !== -1) {
      this.activeMenu = 0;
      this.menus[0].active = true;
    } else if (this.router.url.indexOf('proceedings') !== -1) {
      this.activeMenu = 1;
      this.menus[1].active = true;
    } else if (this.router.url.indexOf('resolution') !== -1) {
      this.activeMenu = 2;
      this.menus[2].active = true;
    }
  }

  clearMenu(){
    this.menus[this.activeMenu].active = false
  }
}


class MenuItem{
  active: boolean
  icon: string;
  title: string;
  route: string;

  constructor(icon: string, title: string, active: boolean){
    this.icon = icon;
    this.title = title;
    this.active = active;
  }
}