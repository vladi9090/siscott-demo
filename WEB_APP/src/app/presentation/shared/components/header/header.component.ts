import { Component, OnInit, Inject, Input } from '@angular/core';
import { Router } from '@angular/router';


/**
 * Componente que se encarga de renderizar la pantalla de login
 */
@Component({
  selector: 'viaje-seguro-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title: string = '';

  constructor(private router: Router) { }

  ngOnInit() {
    if (this.router.url.indexOf('dashboard') !== -1) {
      this.title = 'Dashboard'
    } else if (this.router.url.indexOf('proceedings') !== -1) {
      this.title = 'Expedientes'
    } else if (this.router.url.indexOf('resolution') !== -1) {
      this.title = 'Resoluciones'
    }
  }
}
