import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GetParametro } from '@app/infrastructure/dto/get-app-parametro';
import { ResolutionRepositoryImpl } from '@app/infrastructure/resolution.repository';

export interface DialogData {
  GrupoResolutor: number;
  TipoResolucion: number;
  TipoPlantilla: number;
}


@Component({
  selector: 'app-generar-resolucion',
  templateUrl: './generar-resolucion.component.html',
  styleUrls: ['./generar-resolucion.component.scss'],
  providers: [ResolutionRepositoryImpl]
})

export class GenerarResolucionComponent implements OnInit {

  getTipoResoluccion: GetParametro[] = [];
  getTipoPlantillaResoluccion: GetParametro[] = [];
  constructor(public dialogRef: MatDialogRef<GenerarResolucionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    , private snackBar: MatSnackBar, private service: ResolutionRepositoryImpl) {

    this.ListarTipoResolucion();
  }

  ngOnInit(): void {
  }

  ListarTipoResolucion() {

    this.service.getTipoResolucion().subscribe(resp => {
      console.log(resp)
      if (resp) {
        console.log(resp.items)
        this.getTipoResoluccion = resp.items;
      } else {
        console.log("Tipo Resolucion", resp)
      }
    })

  }

  getTipoPlantilla() {
    let { TipoResolucion } = this.data;
    this.service.getTipoPlantillaResolucion(Number(TipoResolucion)).subscribe(resp => {
      console.log(resp)
      if (resp) {
        console.log(resp.items)

        this.getTipoPlantillaResoluccion = resp.items;

      } else {
        console.log("Tipo Plantilla Resolucion", resp)
      }
    })


  }

  Close() {
    this.dialogRef.close(-1);
  }

  Aceptar() {
    let { GrupoResolutor, TipoResolucion, TipoPlantilla } = this.data;

    console.log(this.data)

    if (GrupoResolutor === 0 || TipoResolucion === 0 || TipoPlantilla === 0) {

      this.snackBar.open("Tiene que ingresar todos campos requeridos", "Advetencia")
      return;
    }

    this.dialogRef.close(this.data);
  }

}
