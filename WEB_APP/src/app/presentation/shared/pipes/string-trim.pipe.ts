import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'stringTrim'})
export class StringTrimPipe implements PipeTransform{

    transform(value: string, max: number) {
        return value.length > max ? `${value.substring(0, max)}...` : value ;
    }
}