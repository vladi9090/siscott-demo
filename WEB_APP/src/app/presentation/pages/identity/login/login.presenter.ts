import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import {
  LoginUserCommand,
  LoginUserCommandHandler,
} from "src/app/application/commands/login-user.command";

@Injectable()
export class LoginPresenter {
  errorMessage: string = "";
  isLoading: boolean = false;

  constructor(
    private loginUserCommand: LoginUserCommand,
    private loginUserCommandHandler: LoginUserCommandHandler,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  loginUser(email: string, password: string) {
    this.resetState();
    this.isLoading = true;
    this.loginUserCommand.email = email;
    this.loginUserCommand.password = password;

    // this.loginUserCommandHandler
    //   .handle(this.loginUserCommand)
    //   .then((value) => this.router.navigate(["/home/proceedings"]))
    //   .catch((error) => this.showError(error.message));
    //this.router.navigate(["/home/proceedings"])

    this.loginUserCommandHandler
      .handle(this.loginUserCommand)
      .then((value) => {
        if (value.status && value.code > 0) {

          this.router.navigate(["/home/proceedings"])
        } else {

          this.snackBar.open(value.message, '', {
            duration: 3000
          });
        }
      })
      .catch((error) => this.showError(error.message));
  }

  showError(errorMessage: string) {
    this.isLoading = false;
    this.snackBar.open(errorMessage, '', {
      duration: 3000
    });
  }

  resetState() {
    this.errorMessage = "";
  }
}
