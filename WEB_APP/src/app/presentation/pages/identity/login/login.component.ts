import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginPresenter } from './login.presenter';

/**
 * Componente que se encarga de renderizar la pantalla de login
 */
@Component({
  selector: 'viaje-seguro-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginPresenter]
})
export class LoginComponent{

  //This is the formgroup that acts as a model for the login's UI Form
  loginForm: FormGroup;
  isError: boolean = false;

  constructor(
    private fb: FormBuilder,
    public presenter: LoginPresenter
  ) {
    this.createForm();
  }


  /**
   * Instantiates the loginForm (FormGroup) with the fields and validation rules
   */
  createForm() {
    this.loginForm = this.fb.group(
      {
        username: ['', [Validators.required]],
        password: ['', Validators.required]
      }
    );
  }

  /**
   * Sends the login form inputs to the server by calling the presenter's login() method
   */
  async onSubmit() {
    this.presenter.loginUser(this.loginForm.get('username')?.value, this.loginForm.get('password')?.value);
  }


}
