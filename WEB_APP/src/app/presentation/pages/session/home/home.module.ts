import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/presentation/shared/shared.module';


export const routes: Routes = [

  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'proceedings', loadChildren: () =>
          import('@app/presentation/pages/session/proceedings/proceedings.module').then(
            (mod) => mod.ProceedingsModule
          )
      },
      {
        path: 'dashboard', loadChildren: () =>
          import('@app/presentation/pages/session/dashboard/dashboard.module').then(
            (mod) => mod.DashboardModule
          )
      },
    ]
  }
]

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
