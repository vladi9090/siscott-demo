import { Component, OnInit, Inject } from "@angular/core";

import { HomePresenter } from "./home.presenter";

/**
 * Componente que se encarga de renderizar la pantalla de login
 */
@Component({
  selector: "viaje-seguro-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  providers: [HomePresenter],
})
export class HomeComponent {
  title: string = "";

  constructor(private presenter: HomePresenter) {}

  onNavigate(event: any) {
    this.title = event;
  }
}
