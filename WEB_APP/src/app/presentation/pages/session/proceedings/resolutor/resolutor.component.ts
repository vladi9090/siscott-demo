import { Component, OnInit} from '@angular/core';
import { ResolutorPresenter } from './resolutor.presenter';


@Component({
  selector: 'siscott-resolutor',
  templateUrl: './resolutor.component.html',
  styleUrls: ['./resolutor.component.scss'],
  providers: [ResolutorPresenter]
})
export class ResolutorComponent  implements OnInit{
  filterName: string = "";
  filterDNI: string = "";
  

  constructor( public presenter: ResolutorPresenter,) {
    
  }

  ngOnInit(): void {
    this.presenter.getAppUsers();
  }


  onPageChanged(event: any): void {
    this.presenter.currentPage = event.pageIndex;
    this.presenter.getAppUsers();
  }
 

  onSearchAppUsers() {
    this.presenter.currentPage = 0;
    this.presenter.getAppUsers(this.filterName, this.filterDNI);
  }

}
