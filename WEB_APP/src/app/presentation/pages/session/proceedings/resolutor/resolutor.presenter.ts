import { Injectable } from "@angular/core";
import { AppUser } from "@app/domain/models/app-user";

import {
  GetAppUsersQuery,
  GetAppUsersQueryHandler,
} from "src/app/application/queries/get-app-users.query";

@Injectable()
export class ResolutorPresenter {
  isLoading: boolean = false;
  displayedColumns: string[] = [ "Nombres", "DNI", "Email"];
  resultsLength = 0;
  tableData: Array<AppUser>;
  currentPage: number = 0;
  rowsPerPage: number = 50;
  errorMessage: string = '';

  name?: string = "";
  dni?: string = "";

  constructor(
    private getAppUsersQuery: GetAppUsersQuery,
    private getAppUsersQueryHandler: GetAppUsersQueryHandler
  ) {}

  getAppUsers(name?: string, dni?: string) {
    // if(name){
    //   this.name = name;
    //   this.dni = dni;
    // }

    // this.getAppUsersQuery.name = this.name;
    // this.getAppUsersQuery.dni = this.dni;

    this.isLoading = true;
    this.getAppUsersQueryHandler
      .handle(this.getAppUsersQuery)
      .then((appUsers) => {
        this.isLoading = false;
        this.resultsLength = appUsers.length > 0 ? appUsers[0].total : 0;
        this.tableData = appUsers;
      })
      .catch((error) => {
        this.isLoading = false;
        this.errorMessage = error.message;
      });
  }

  
}
