import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/presentation/shared/shared.module';
import { ResolutorComponent } from './resolutor/resolutor.component';
import { ResolutionComponent } from './resolution/resolution.component';


export const routes: Routes = [

  {
    path: '',
    redirectTo: 'resolutor',
    pathMatch: 'full'
  },
  {
    path: 'resolutor',
    component: ResolutorComponent
  },
  {
    path: 'resolution',
    component: ResolutionComponent
  },
]

@NgModule({
  declarations: [
    ResolutorComponent,
    ResolutionComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    //  FormsModule,
    //  ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    ResolutorComponent,
    ResolutionComponent
  ]
})
export class ProceedingsModule { }
