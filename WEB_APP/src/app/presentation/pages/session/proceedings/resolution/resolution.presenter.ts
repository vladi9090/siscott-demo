import { SelectionModel } from "@angular/cdk/collections";
import { Injectable } from "@angular/core";
import { ResolutionCommand } from "@app/application/commands/resolution.command";
import { AppResolucionAdministrativa } from "@app/domain/models/app-resolution";
import { ResolutionCommandHandler } from '../../../../../application/commands/resolution.command';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  GetAppResolucionAdministrativaQuery,
  GetAppResolucionAdministrativaQueryHandler,
} from "src/app/application/queries/get-app-resolutions.query";
import { GetAppGrupoFormatoQueryHandler } from "@app/application/queries/get-app-group-format.query";
import { AppGrupoFormato } from "@app/domain/models/app-format-group";
import { GetAppTipoFormatoQuery, GetAppTipoFormatoQueryHandler } 
from "@app/application/queries/get-app-type-format.query";
import { AppTipoFormato } from "@app/domain/models/app-type-format";

import { GetAppTipoServicioQuery, GetAppTipoServicioQueryHandler } from "@app/application/queries/get-app-type-service.query";
import { AppParametro } from "@app/domain/models/app-parameter";
import { GetAppTipoResolucionQueryHandler } from "@app/application/queries/get-app-type-resolution.query";
import { AppGrupoResolutor } from "@app/domain/models/app-group-resolutor";
import { GetAppGrupoResolutorQueryHandler } from "@app/application/queries/get-app-group-resolution.query";


@Injectable()
export class ResolutionPresenter {
  isLoading: boolean = false;
  displayedColumns: string[] = ["select", "numero_acta", "fecha_acta", "codigo_falta", "administrado_acta", "estado_pago", "numero_resolucion", "fecha_resolucion", "notificacion","accion"];
  resultsLength = 0;
  tableData: Array<AppResolucionAdministrativa>;
  DataGrupoFormato: Array<AppGrupoFormato>;
  DataTipoFormato: Array<AppTipoFormato>;
  DataTipoServicio: Array<AppParametro>;
  DataTipoResolucion: Array<AppParametro>;
  DataGrupoResolutor: Array<AppGrupoResolutor>;
  currentPage: number = 0;
  rowsPerPage: number = 50;
  errorMessage: string = '';


  GrupoFormato: number = 0;
  TipoResolucion: number = 0;
  TipoServicio: number = 0;
  TipoFalta: number = 0;
  GrupoResolutor: number = 0;
  NumeroActas: string = "";
  Administrado: string = "";
  NumeroExpediente: string = "";
  IdTipoFormato: number = 0;

  selection = new SelectionModel<AppResolucionAdministrativa>(true, []);
  constructor(
    private getAppResolucionAdministrativaQuery: GetAppResolucionAdministrativaQuery,
    private getAppResolucionAdministrativaQueryHandler: GetAppResolucionAdministrativaQueryHandler,
    private resolutionCommand: ResolutionCommand,
    private ResolutionCommandHandler: ResolutionCommandHandler,
    private snackBar: MatSnackBar,
    private getAppGrupoFormatoQueryHandler: GetAppGrupoFormatoQueryHandler,
    private getAppTipoFormatoQuery: GetAppTipoFormatoQuery,
    private getAppTipoFormatoQueryHandler: GetAppTipoFormatoQueryHandler,
    private getAppTipoServicioQuery: GetAppTipoServicioQuery,
    private getAppTipoServicioQueryHandler: GetAppTipoServicioQueryHandler,
    private getAppTipoResolucionQueryHandler: GetAppTipoResolucionQueryHandler,
    private getAppGrupoResolutorQueryHandler: GetAppGrupoResolutorQueryHandler
  ) {}

  getAppResolutions(
    GrupoFormato: number = 0,
    TipoResolucion: number = 0,
    TipoServicio: number = 0,
    TipoFalta: number = 0,
    GrupoResolutor: number = 0,
    NumeroActas: string = "",
    Administrado: string = "",
    NumeroExpediente: string = ""
  ) {

    //if (GrupoFormato || TipoResolucion || TipoServicio || TipoFalta || GrupoResolutor || NumeroActas || Administrado || NumeroExpediente) {
    console.log(GrupoFormato + "----" + "TipoResolucion" + "----" + TipoServicio + "----" +  TipoFalta + "----" + GrupoResolutor + "----" + NumeroActas + "----" + Administrado + "----" + NumeroExpediente);
    this.GrupoFormato = GrupoFormato;
    this.TipoResolucion = TipoResolucion;
    this.TipoServicio = TipoServicio;
    this.TipoFalta = TipoFalta;
    this.GrupoResolutor = GrupoResolutor;
    this.NumeroActas = NumeroActas;
    this.Administrado = Administrado;
    this.NumeroExpediente = NumeroExpediente;
    // }
    this.getAppResolucionAdministrativaQuery.GrupoFormato = this.GrupoFormato;
    this.getAppResolucionAdministrativaQuery.TipoResolucion = this.TipoResolucion;
    this.getAppResolucionAdministrativaQuery.TipoServicio = this.TipoServicio;
    this.getAppResolucionAdministrativaQuery.TipoFalta = this.TipoFalta;
    this.getAppResolucionAdministrativaQuery.GrupoResolutor = this.GrupoResolutor;
    this.getAppResolucionAdministrativaQuery.NumeroActas = this.NumeroActas;
    this.getAppResolucionAdministrativaQuery.Administrado = this.Administrado;
    this.getAppResolucionAdministrativaQuery.NumeroExpediente = this.NumeroExpediente;

    this.isLoading = true;
    this.getAppResolucionAdministrativaQueryHandler
      .handle(this.getAppResolucionAdministrativaQuery)
      .then((AppResolucionAdministrativa) => {
        this.isLoading = false;
        this.resultsLength = AppResolucionAdministrativa.length > 0 ? AppResolucionAdministrativa[0].total : 0;
        this.tableData = AppResolucionAdministrativa;
      })
      .catch((error) => {
        this.isLoading = false;
        this.errorMessage = error.message;
      });

  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.tableData.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.tableData.forEach(row => this.selection.select(row));
  }

  evGenerarPlantilla(parametro: { GrupoResolutor: string, TipoResolucion: string, TipoPlantilla: string }) {

    console.log('parametro', parametro)
    console.log('this.selection', this.selection.selected)

    this.resolutionCommand.id_usuario_reg = "Admin",
      this.resolutionCommand.id_dependencia = 3;
    this.resolutionCommand.id_tipo_resolucion = Number.parseInt(parametro.TipoResolucion);
    this.resolutionCommand.id_tipo_plantilla = Number.parseInt(parametro.TipoPlantilla);
    this.resolutionCommand.id_grupo_resolutor = Number.parseInt(parametro.GrupoResolutor);

    this.selection.selected.forEach(item => {


      this.resolutionCommand.id_documento_control = item.id_documento_control;
      this.resolutionCommand.id_entidad = item.id_entidad;
      this.resolutionCommand.secuencia_detalle = item.secuencia;
      this.resolutionCommand.id_falta = item.id_falta;
      // this.resolutionCommand.id_entidad = item.id_entidad,

    })

    this.ResolutionCommandHandler.handle(this.resolutionCommand)
      .then((value) => {
        if (value.status && value.code > 0) {
          this.snackBar.open("Se registró correctamente resolución - " + value.message, "Advertencia")
          this.getAppResolutions()
        } else {
          this.snackBar.open(value.message, "Advertencia")
        }
      })
      .catch((error) => {
        this.snackBar.open("Servicio no disponible", "Error")
      })

  }

  getAppGrupoFormato(){
    this.getAppGrupoFormatoQueryHandler
      .handle()
      .then((AppGrupoFormato) => {
        this.DataGrupoFormato = AppGrupoFormato;
      })
      .catch((error) => {
        this.errorMessage = error.message;
      });
  }

  eventDescargarResolucion(value: number) {
    console.log()
  }

  onChangeSelected(Id: number = 0, value: number){

      if(value == 0)
      {  
          this.getAppTipoFormatoQuery.id_grupo_formato = Id;
          this.getAppTipoFormatoQueryHandler
          .handle(this.getAppTipoFormatoQuery)
          .then((AppTipoFormato) => {
            this.DataTipoFormato = AppTipoFormato;
          })
          .catch((error) => {
            this.errorMessage = error.message;
          });
      }
      else if(value == 1)
      {
        this.getAppTipoServicioQuery.id_tipo_formato = Id;
        this.getAppTipoServicioQueryHandler
        .handle(this.getAppTipoServicioQuery)
        .then((AppTipoServicio) => {
          this.DataTipoServicio = AppTipoServicio;
        })
        .catch((error) => {
          this.errorMessage = error.message;
        });
      }   
  }

  getAppTipoResolucion(){
    this.getAppTipoResolucionQueryHandler
      .handle()
      .then((AppTipoResolucion) => {
        this.DataTipoResolucion = AppTipoResolucion;
      })
      .catch((error) => {
        this.errorMessage = error.message;
      });
  }

  getAppGrupoResolutor(){
    this.getAppGrupoResolutorQueryHandler
      .handle()
      .then((AppGrupoResolutor) => {
        this.DataGrupoResolutor = AppGrupoResolutor;
      })
      .catch((error) => {
        this.errorMessage = error.message;
      });
  }
}

