import { Component, OnInit } from '@angular/core';
import { ResolutionPresenter } from './resolution.presenter';
import { PdfMakeWrapper, Txt } from 'pdfmake-wrapper';
import { MatDialog } from '@angular/material/dialog';
import { GenerarResolucionComponent } from '../../../../shared/components/modal/resolucion/generar-resolucion/generar-resolucion.component';


@Component({
  selector: 'app-resolution',
  templateUrl: './resolution.component.html',
  styleUrls: ['./resolution.component.scss'],
  providers: [ResolutionPresenter]
})

export class ResolutionComponent implements OnInit {
  filterTipoFormato: number = 0;
  filterGrupoFormato: number = 0;
  filterTipoResolucion: number = 0;
  filterTipoServicio: number = 0;
  filterTipoFalta: number = 0;
  filterGrupoResolutor: number = 0;
  filterNumeroActas: string = "";
  filterAdministrado: string = "";
  filterNumeroExpediente: string = "";

  constructor(public presenter: ResolutionPresenter, public dialog: MatDialog) { }


  ngOnInit(): void {
    this.presenter.getAppResolutions();
    this.presenter.getAppGrupoFormato();
    this.presenter.getAppTipoResolucion();
    this.presenter.getAppGrupoResolutor();
  }


  onPageChanged(event: any): void {
    this.presenter.currentPage = event.pageIndex;
    this.presenter.getAppResolutions();
  }


  onSearchAppResolucionAdministrado() {
    this.presenter.currentPage = 0;
    this.presenter.getAppResolutions(
      this.filterGrupoFormato,
      this.filterTipoResolucion,
      this.filterTipoServicio,
      this.filterTipoFalta,
      this.filterGrupoResolutor,
      this.filterNumeroActas,
      this.filterAdministrado,
      this.filterNumeroExpediente);
  }

  generatePdf() {
    const pdf = new PdfMakeWrapper();
    pdf.add(
      new Txt("Pdf de prueba.").bold().italics().end
    );
    pdf.create().open();
  }


  onGenerarResolucion() {
    // GrupoResolutor: number;
    // TipoResolucion: number;
    // TipoPlantilla: number;
    const dialogRef = this.dialog.open(GenerarResolucionComponent, {
      data: { GrupoResolutor: 0, TipoResolucion: 0, TipoPlantilla: 0 },
      width: '40%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {

      if (result !== null && result != -1) {

        this.presenter.evGenerarPlantilla(result);

      }
    })
  }

  

}



