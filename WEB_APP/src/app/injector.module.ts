import { UserRepositoryImpl } from './infrastructure/user.respository';
import { UserRepository } from './domain/repositories/user.repository';
import { LoginUserCommand, LoginUserCommandHandler } from './application/commands/login-user.command';
import { ApiService } from './infrastructure/remote/api.service';
import { BrowserStorage } from './infrastructure/local/browser.storage';
import { NgModule } from '@angular/core';
import { GetAppUsersQuery, GetAppUsersQueryHandler } from './application/queries/get-app-users.query';
import { ProceedingsRepository } from './domain/repositories/proceedings.repository';
import { ProceedingsRepositoryImpl } from './infrastructure/proceedings.repository';
import { GetAppResolucionAdministrativaQuery, GetAppResolucionAdministrativaQueryHandler } from './application/queries/get-app-resolutions.query';
import { ResolutionCommandHandler, ResolutionCommand } from './application/commands/resolution.command';
import { ResolutionRepository } from './domain/repositories/resolution.repository';
import { ResolutionRepositoryImpl } from './infrastructure/resolution.repository';
import { GetAppTipoFormatoQuery, GetAppTipoFormatoQueryHandler } from '@app/application/queries/get-app-type-format.query';
import { GetAppTipoServicioQuery, GetAppTipoServicioQueryHandler } from '@app/application/queries/get-app-type-service.query';
import { GetAppGrupoFormatoQueryHandler } from '@app/application/queries/get-app-group-format.query';
import { GetAppTipoResolucionQueryHandler } from './application/queries/get-app-type-resolution.query';
import { GetAppGrupoResolutorQueryHandler } from './application/queries/get-app-group-resolution.query';

@NgModule({
  imports: [
  ],
  providers: [
    {provide: UserRepository, useClass: UserRepositoryImpl},
    {provide: ProceedingsRepository, useClass: ProceedingsRepositoryImpl},
    {provide: ResolutionRepository, useClass: ResolutionRepositoryImpl},
    BrowserStorage,
    LoginUserCommand,
    LoginUserCommandHandler,
    GetAppUsersQuery,
    GetAppUsersQueryHandler,
    GetAppResolucionAdministrativaQuery,
    GetAppResolucionAdministrativaQueryHandler,
    GetAppTipoFormatoQuery,
    GetAppTipoFormatoQueryHandler,
    GetAppGrupoFormatoQueryHandler,
    GetAppTipoServicioQuery,
    GetAppTipoServicioQueryHandler,
    GetAppTipoResolucionQueryHandler,
    GetAppGrupoResolutorQueryHandler,
    ApiService,
    
    ResolutionCommand,
    ResolutionCommandHandler,
    
  ]
})
export class InjectorModule { }
