
import { Injectable } from "@angular/core";
import { Failure } from "src/app/domain/error/failure";
import { User } from "src/app/domain/models/user";
import { UserRepository } from "src/app/domain/repositories/user.repository";
import { BaseCommandHandler } from "./base.handler";
import { DataRequestDto, RequestDto } from '../../infrastructure/dto/get-data-request';

@Injectable()
export class LoginUserCommand {
    public email: string
    public password: string

    constructor() { }
}

@Injectable()
export class LoginUserCommandHandler implements
    BaseCommandHandler<Promise<RequestDto>, LoginUserCommand>{

    constructor(private userRepository: UserRepository) { }

    // handle(command: LoginUserCommand): Promise<void> {
    //     return new Promise((resolve, reject) => {
    //         this.userRepository.loginUser(command.email, command.password)
    //             .subscribe(
    //                 (user: User) => {
    //                     if (user.isLoggedIn()) {
    //                         console.log("User in command", user)
    //                         //saving the user token to the session storage
    //                         this.userRepository.saveUserSession(user);
    //                         resolve()
    //                     } else {
    //                         reject()
    //                     }
    //                 },
    //                 (error: Failure) => {
    //                     reject(error);
    //                 }
    //             );
    //     });
    // }

    handle(command: LoginUserCommand): Promise<RequestDto> {
        return new Promise((resolve, reject) => {
            this.userRepository.loginUserCustom(command.email, command.password)
                .subscribe(
                    (user: DataRequestDto<User>) => {

                        const resp: RequestDto = ({
                            ...user
                        })
                        if (user.status) {
                            console.log("User in command", user)
                            //saving the user token to the session storage
                            this.userRepository.saveUserSession(user.data);

                            resolve(resp)
                        } else {
                            reject(resp)
                        }

                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }

}