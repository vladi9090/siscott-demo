import { Injectable } from "@angular/core";
import { ResolutionRepository } from "@app/domain/repositories/resolution.repository";
import { reject } from "lodash";
import { Failure } from "src/app/domain/error/failure";
import { BaseCommandHandler } from './base.handler';
import { AppRequest } from '../../domain/models/app-request';
@Injectable()
export class ResolutionCommand {
    public id_entidad: number;
    public id_dependencia: number;
    public id_tipo_resolucion: number;
    public id_grupo_resolutor: number;
    public id_usuario_reg: string;
    public id_documento_control: number;
    public secuencia_detalle: number;
    public id_falta: number;
    public id_tipo_plantilla: number;
}

@Injectable()

export class ResolutionCommandHandler implements BaseCommandHandler<Promise<AppRequest>, ResolutionCommand> {

    constructor(private resolutionRepository: ResolutionRepository) { }

    handle(command: ResolutionCommand): Promise<AppRequest> {

        return new Promise((resolve, reject) => {
            this.resolutionRepository.postGenerarResolucion(command)
                .subscribe((resp: AppRequest) => {
                    resolve(resp)
                }, (error: Failure) => {
                    reject(error);
                })
        })
    }

}