export interface BaseQueryHandler <R, T>{
    handle(command: T): R
}
