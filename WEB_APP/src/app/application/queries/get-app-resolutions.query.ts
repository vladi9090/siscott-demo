import { Injectable } from "@angular/core";
import { AppResolucionAdministrativa } from "@app/domain/models/app-resolution";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { BaseQueryHandler } from "./base.query";

@Injectable()
export class GetAppResolucionAdministrativaQuery {
    GrupoFormato: number;
    TipoResolucion: number;
    TipoServicio: number ;
    TipoFalta: number;
    GrupoResolutor: number;
    NumeroActas: string;
    Administrado: string; 
    NumeroExpediente: string;
    constructor(){}
}
 

@Injectable()
export class GetAppResolucionAdministrativaQueryHandler implements BaseQueryHandler<Promise<Array<AppResolucionAdministrativa>>, GetAppResolucionAdministrativaQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(query: GetAppResolucionAdministrativaQuery): Promise<Array<AppResolucionAdministrativa>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppResolucionAdministrativa(
            query.GrupoFormato, query.TipoResolucion, query.TipoServicio,
            query.TipoFalta, query.GrupoResolutor, query.NumeroActas, query.Administrado, query.NumeroExpediente
            )
                .subscribe(
                    (appResolucionAdministrativa: Array<AppResolucionAdministrativa>) => {
                        resolve(appResolucionAdministrativa)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }

}


