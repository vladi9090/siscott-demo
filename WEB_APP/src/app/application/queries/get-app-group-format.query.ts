import { Injectable } from "@angular/core";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { BaseQueryHandler } from "./base.query";
import { AppGrupoFormato } from '../../domain/models/app-format-group';

@Injectable()
export class GetAppGrupoFormatoQuery { constructor(){} }

@Injectable()
export class GetAppGrupoFormatoQueryHandler implements BaseQueryHandler<Promise<Array<AppGrupoFormato>>, GetAppGrupoFormatoQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(): Promise<Array<AppGrupoFormato>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppGrupoFormato()
                .subscribe(
                    (appGrupoFormato: Array<AppGrupoFormato>) => {
                        resolve(appGrupoFormato)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }
}

