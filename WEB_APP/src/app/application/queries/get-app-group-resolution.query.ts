import { Injectable } from "@angular/core";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { BaseQueryHandler } from "./base.query";
import { AppGrupoResolutor } from '../../domain/models/app-group-resolutor';

@Injectable()
export class GetAppGrupoResolutorQuery { constructor(){} }

@Injectable()
export class GetAppGrupoResolutorQueryHandler implements BaseQueryHandler<Promise<Array<AppGrupoResolutor>>, GetAppGrupoResolutorQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(): Promise<Array<AppGrupoResolutor>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppGrupoResolutor()
                .subscribe(
                    (appGrupoResolutor: Array<AppGrupoResolutor>) => {
                        resolve(appGrupoResolutor)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        }); 
    }
}