
import { Injectable } from "@angular/core";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { AppUser } from "src/app/domain/models/app-user";
import { User } from "src/app/domain/models/user";
import { UserRepository } from "src/app/domain/repositories/user.repository";
import { BaseQueryHandler } from "./base.query";


@Injectable()
export class GetAppUsersQuery {
    name?: string;
    dni?: string;
    constructor(){}
}

@Injectable()
export class GetAppUsersQueryHandler implements BaseQueryHandler<Promise<Array<AppUser>>, GetAppUsersQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(query: GetAppUsersQuery): Promise<Array<AppUser>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppUsers(query.name, query.dni)
                .subscribe(
                    (appUsers: Array<AppUser>) => {
                        resolve(appUsers)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }

}