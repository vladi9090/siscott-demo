import { Injectable } from "@angular/core";
import { AppParametro } from "@app/domain/models/app-parameter";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { BaseQueryHandler } from "./base.query";

@Injectable()
export class GetAppTipoResolucionQuery { 
    id_tipo_resolucion:     number;
    
    constructor(){} 
}

@Injectable()
export class GetAppTipoResolucionQueryHandler implements BaseQueryHandler<Promise<Array<AppParametro>>, GetAppTipoResolucionQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(): Promise<Array<AppParametro>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppTipoResolucion()
                .subscribe(
                    (appTipoServicio: Array<AppParametro>) => {
                        resolve(appTipoServicio)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }
}