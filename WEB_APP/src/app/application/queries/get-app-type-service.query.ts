import { Injectable } from "@angular/core";
import { AppParametro } from "@app/domain/models/app-parameter";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { BaseQueryHandler } from "./base.query";

@Injectable()
export class GetAppTipoServicioQuery { 
    id_tipo_formato:     number;
    
    constructor(){} 
}

@Injectable()
export class GetAppTipoServicioQueryHandler implements BaseQueryHandler<Promise<Array<AppParametro>>, GetAppTipoServicioQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(query: GetAppTipoServicioQuery): Promise<Array<AppParametro>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppTipoServicio(query.id_tipo_formato)
                .subscribe(
                    (appTipoServicio: Array<AppParametro>) => {
                        resolve(appTipoServicio)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }
}