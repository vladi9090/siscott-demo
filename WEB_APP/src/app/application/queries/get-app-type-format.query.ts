import { Injectable } from "@angular/core";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Failure } from "src/app/domain/error/failure";
import { BaseQueryHandler } from "./base.query";
import { AppTipoFormato } from "@app/domain/models/app-type-format";

@Injectable()
export class GetAppTipoFormatoQuery { 
    id_grupo_formato:     number;
    
    constructor(){} 
}

@Injectable()
export class GetAppTipoFormatoQueryHandler implements BaseQueryHandler<Promise<Array<AppTipoFormato>>, GetAppTipoFormatoQuery>{

    constructor(private proceedingsRepository: ProceedingsRepository) { }

    handle(query: GetAppTipoFormatoQuery): Promise<Array<AppTipoFormato>> {
        return new Promise((resolve, reject) => {
            this.proceedingsRepository.getAppTipoFormato(query.id_grupo_formato)
                .subscribe(
                    (appTipoFormato: Array<AppTipoFormato>) => {
                        resolve(appTipoFormato)
                    },
                    (error: Failure) => {
                        reject(error);
                    }
                );
        });
    }
}