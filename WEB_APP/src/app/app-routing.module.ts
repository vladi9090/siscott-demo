import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'identity', pathMatch: 'full' },
  {
    path: 'identity',
    loadChildren: () =>
      import('./presentation/pages/identity/identity.module').then(
        (mod) => mod.IdentityModule
      ),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./presentation/pages/session/home/home.module').then(
        (mod) => mod.HomeModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
