export class AppTipoFormato{  
    id_tipo_formato:  number;
    nombre_formato:   string;
    fecha_reg:        Date;
    id_usuario_reg:   null;
    fecha_mod:        Date;
    id_usuario_mod:   null;
    estado:           null;
    id_grupo_formato: number;
    total:            number;

    constructor() { }
}