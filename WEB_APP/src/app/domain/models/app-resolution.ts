export class AppResolucionAdministrativa{        
        id_documento_control:        number;
        numero_acta:                 string;
        fecha_acta:                  Date;
        administrado_acta:           string;
        codigo_falta:                string;
        id_resolucion:               number;
        descripcion_tipo_resolucion: string;
        numero_resolucion:           string;
        administrado_resolucion:     string;
        fecha_resolucion:            Date;
        usu_reg_resolucion:          string;
        estado_pago:                 null;
        id_notificacion:             number;
        notificacion:                null;
        total:                       number;
        secuencia:                   number;
        id_falta:                    number;
        id_entidad:                  number;
        id_documento_externo:        number;
    constructor() { }
}
