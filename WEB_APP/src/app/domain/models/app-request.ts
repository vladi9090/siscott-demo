export class AppRequest {
    code:                  number;
    message:               string;
    iDbdGenerado:          number;
    status:                boolean;
}

export class AppDataRequest<T> extends AppRequest {
    data: T;
}
