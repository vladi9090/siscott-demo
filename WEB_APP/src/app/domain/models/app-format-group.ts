export class AppGrupoFormato{  
    id_grupo_formato:     number;
    nombre_grupo_formato: string;
    estado:               null;
    total:                number;

    constructor() { }
}