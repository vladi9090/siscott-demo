export class AppUser {
    idUsuario: number;
    nombre: string
    apellidos: string
    email: string
    password: string
    dni: string
    total: number
    firebaseToken?: string
    
    constructor() { }

}