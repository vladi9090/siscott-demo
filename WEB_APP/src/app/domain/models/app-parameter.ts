export class AppParametro{  
    id_parametro:       number;
    id_grupo_parametro: number;
    valor:              string;
    orden:              number;
    id_tipo_formato:    number;
    sigla:              null;
    total:              number;

    constructor() { }
}