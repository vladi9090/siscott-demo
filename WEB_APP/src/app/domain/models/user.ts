export class User{
    idUsuario: number
    nombre: string
    apellidos: string
    email: string 
    telefono: string
    accessToken?: string
    accessTokenDuration: number
    firebaseToken: string
    dni: string

    constructor(){}

    isLoggedIn(){
        return this.accessToken !== '';
    }
}