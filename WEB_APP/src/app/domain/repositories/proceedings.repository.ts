import { Observable } from "rxjs";
import { AppUser } from "../models/app-user";
import { AppResolucionAdministrativa } from '../models/app-resolution';
import { AppTipoFormato } from '@app/domain/models/app-type-format';
import { AppGrupoFormato } from '@app/domain/models/app-format-group';
import { AppParametro } from '@app/domain/models/app-parameter';
import { AppGrupoResolutor } from '@app/domain/models/app-group-resolutor';


export abstract class ProceedingsRepository {
  getAppUsers: (name?: string, dni?: string) => Observable<Array<AppUser>>;
  getAppResolucionAdministrativa: (GrupoFormato?: number, TipoResolucion?: number, TipoServicio?: number, TipoFalta?: number, GrupoResolutor?: number, NumeroActas?: string, Administrado?: string, NumeroExpediente?: string) => Observable<Array<AppResolucionAdministrativa>>;
  getAppGrupoFormato: () => Observable<Array<AppGrupoFormato>>;
  getAppTipoFormato: (idGrupoFormato: number) => Observable<Array<AppTipoFormato>>;
  getAppTipoServicio: (idTipoFormato: number) => Observable<Array<AppParametro>>;
  getAppTipoResolucion: () => Observable<Array<AppParametro>>;
  getAppGrupoResolutor: () => Observable<Array<AppGrupoResolutor>>;
}