import { ResolutionCommand } from "@app/application/commands/resolution.command";
import { AppRequest } from "../models/app-request";
import { Observable } from 'rxjs';

export abstract class ResolutionRepository {
    postGenerarResolucion: (command: ResolutionCommand) => Observable<AppRequest>;
}
