import { Observable } from "rxjs";
import { AppUser } from "../models/app-user";
import { User } from "../models/user";
import { DataRequestDto } from '../../infrastructure/dto/get-data-request';

export abstract class UserRepository {
  loginUser: (email: string, password: string) => Observable<User>;
  saveUserSession: (user: User) => void;
  getUserSession: () => User;
  getAppUsers: (name: string, dni: string) => Observable<Array<AppUser>>;
  loginUserCustom: (email: string, password: string) => Observable<DataRequestDto<User>>;
}
