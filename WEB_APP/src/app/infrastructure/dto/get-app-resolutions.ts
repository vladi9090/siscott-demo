import { AppResolucionAdministrativa } from "src/app/domain/models/app-resolution";

export class GetAppResolucionesAdministradoDto {
    hasItems: boolean;
    items: Array<GetAppResolucionesAdministradoDataDto>;
    total: number;
    page: number;
    pages: number;
    constructor() { };
}

export class GetAppResolucionesAdministradoDataDto {
    id_documento_control: number;
    numero_acta: string;
    fecha_acta: Date;
    administrado_acta: string;
    codigo_falta: string;
    id_resolucion: number;
    descripcion_tipo_resolucion: string;
    numero_resolucion: string;
    administrado_resolucion: string;
    fecha_resolucion: Date;
    usu_reg_resolucion: string;
    estado_pago: null;
    id_notificacion: number;
    notificacion: null;
    total: number;
    id_falta: number;
    id_entidad: number;
    secuencia: number;
    id_documento_externo: number;

    constructor() { };
}

export class GetResolucionesAdministradoDtoTransformer {

    constructor() { }

    transform(dto: GetAppResolucionesAdministradoDto): Array<AppResolucionAdministrativa> {

        var appResoluciones = dto.items.map((resolucionData) => {
            const appResolucionAdmin = new AppResolucionAdministrativa();

            appResolucionAdmin.id_documento_control = resolucionData.id_documento_control;
            appResolucionAdmin.numero_acta = resolucionData.numero_acta
            appResolucionAdmin.fecha_acta = resolucionData.fecha_acta;
            appResolucionAdmin.administrado_acta = resolucionData.administrado_acta;
            appResolucionAdmin.codigo_falta = resolucionData.codigo_falta;
            appResolucionAdmin.id_resolucion = resolucionData.id_resolucion;
            appResolucionAdmin.descripcion_tipo_resolucion = resolucionData.descripcion_tipo_resolucion;
            appResolucionAdmin.numero_resolucion = resolucionData.numero_resolucion;
            appResolucionAdmin.administrado_resolucion = resolucionData.administrado_resolucion;
            appResolucionAdmin.fecha_resolucion = resolucionData.fecha_resolucion;
            appResolucionAdmin.usu_reg_resolucion = resolucionData.usu_reg_resolucion;
            appResolucionAdmin.estado_pago = resolucionData.estado_pago;
            appResolucionAdmin.id_notificacion = resolucionData.id_notificacion;
            appResolucionAdmin.notificacion = resolucionData.notificacion;
            appResolucionAdmin.total = dto.total;
            appResolucionAdmin.id_falta = resolucionData.id_falta;
            appResolucionAdmin.id_entidad = resolucionData.id_entidad;
            appResolucionAdmin.secuencia = resolucionData.secuencia;
            appResolucionAdmin.id_documento_externo = resolucionData.id_documento_control;
            
            return appResolucionAdmin;
        });

        return appResoluciones;
    }
}