import { User } from "src/app/domain/models/user";

export class UserLoginDto {
    idUsuario: number
    nombre: string;
    apellidos: string;
    email: string;
    telefono: string;
    accessToken?: string;
    expiresIn?: number
    constructor() { };
}

export class UserLoginDtoTransformer {

    constructor() { }

    transform(dto: UserLoginDto): User {
        const user = new User();
        user.idUsuario = dto.idUsuario;
        user.nombre = dto.nombre;
        user.apellidos = dto.apellidos;
        user.email = dto.email;
        user.telefono = dto.telefono;
        user.accessToken = dto.accessToken;

        return user;
    }
}