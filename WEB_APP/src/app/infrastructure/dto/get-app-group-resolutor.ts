import { AppGrupoResolutor } from "@app/domain/models/app-group-resolutor";

export class GetAppGrupoResolutorDto {
    hasItems: boolean;
    items:    Array<GetAppGrupoResolutorDataDto>;
    total:    number;
    page:     number;
    pages:    number;

    constructor() { };
}

export class GetAppGrupoResolutorDataDto {
    id_grupo_resolutor: number;
    descripcion:        string;
    estado:             null;
    sigla:              null;
    
    constructor() { }
}

export class GetGrupoResolutorDtoTransformer {

    constructor() { }

    transform(dto: GetAppGrupoResolutorDto): Array<AppGrupoResolutor> {

        var appGrupoResolutor = dto.items.map((GrupoResolutorData) => {
            const appGrupoResolucion = new AppGrupoResolutor();
            appGrupoResolucion.id_grupo_resolutor = GrupoResolutorData.id_grupo_resolutor;
            appGrupoResolucion.descripcion = GrupoResolutorData.descripcion;
            appGrupoResolucion.total = dto.total;
            
            return appGrupoResolucion;
        });

        return appGrupoResolutor;
    }
}