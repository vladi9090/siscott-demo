import { AppTipoFormato } from "@app/domain/models/app-type-format";

export class GetAppTipoFormatoDto {
    hasItems: boolean;
    items:    Array<GetAppTipoFormatoDataDto>;
    total:    number;
    page:     number;
    pages:    number;

    constructor() { };
}

export class GetAppTipoFormatoDataDto {
    id_tipo_formato:  number;
    nombre_formato:   string;
    fecha_reg:        Date;
    id_usuario_reg:   null;
    fecha_mod:        Date;
    id_usuario_mod:   null;
    estado:           null;
    id_grupo_formato: number;

    constructor() { }
}

export class GetTipoFormatoDtoTransformer {

    constructor() { }

    transform(dto: GetAppTipoFormatoDto): Array<AppTipoFormato> {

        var appTipoFormato = dto.items.map((TipoFormatoData) => {
            const appTipoFormato = new AppTipoFormato();
            appTipoFormato.id_tipo_formato = TipoFormatoData.id_tipo_formato;
            appTipoFormato.nombre_formato = TipoFormatoData.nombre_formato;
            appTipoFormato.estado = TipoFormatoData.estado
            appTipoFormato.total = dto.total;
            
            return appTipoFormato;
        });

        return appTipoFormato;
    }
}