import { AppGrupoFormato } from "@app/domain/models/app-format-group";

export class GetAppGrupoFormatoDto {
    hasItems: boolean;
    items:    Array<GetAppGrupoFormatoDataDto>;
    total:    number;
    page:     number;
    pages:    number;

    constructor() { };
}

export class GetAppGrupoFormatoDataDto {
    id_grupo_formato:     number;
    nombre_grupo_formato: string;
    estado:               null;
    total:                number;

    constructor() { }
}

export class GetGrupoFormatoDtoTransformer {

    constructor() { }

    transform(dto: GetAppGrupoFormatoDto): Array<AppGrupoFormato> {

        var appGrupoFormato = dto.items.map((GrupoFormatoData) => {
            const appGrupoFormato = new AppGrupoFormato();
            appGrupoFormato.id_grupo_formato = GrupoFormatoData.id_grupo_formato;
            appGrupoFormato.nombre_grupo_formato = GrupoFormatoData.nombre_grupo_formato;
            appGrupoFormato.estado =   GrupoFormatoData.estado;
            appGrupoFormato.total = dto.total;

            return appGrupoFormato;
        });

        return appGrupoFormato;
    }
}