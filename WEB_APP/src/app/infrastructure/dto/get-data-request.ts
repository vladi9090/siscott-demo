export class RequestDto {
    code: number;
    message: string;
    iDbdGenerado: number;
    status: boolean;
}


export class DataRequestDto<T> extends RequestDto {
    data: T;
}

