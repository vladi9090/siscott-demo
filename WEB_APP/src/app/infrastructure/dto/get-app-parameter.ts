import { AppParametro } from "@app/domain/models/app-parameter";

export class GetAppParametroDto {
    hasItems: boolean;
    items:    Array<GetAppParametroDataDto>;
    total:    number;
    page:     number;
    pages:    number;

    constructor() { };
}

export class GetAppParametroDataDto {
    id_parametro:       number;
    id_grupo_parametro: number;
    valor:              string;
    orden:              number;
    id_tipo_formato:    number;
    sigla:              null;
    
    constructor() { }
}

export class GetTipoResolucionDtoTransformer {

    constructor() { }

    transform(dto: GetAppParametroDto): Array<AppParametro> {

        var appTipoResolucion = dto.items.map((TipoResolucionData) => {
            const appTipoResolucion = new AppParametro();
            appTipoResolucion.id_parametro = TipoResolucionData.id_parametro;
            appTipoResolucion.valor = TipoResolucionData.valor;
            appTipoResolucion.total = dto.total;
            
            return appTipoResolucion;
        });

        return appTipoResolucion;
    }
}

export class GetTipoServicioDtoTransformer {

    constructor() { }

    transform(dto: GetAppParametroDto): Array<AppParametro> {

        var appTipoServicio = dto.items.map((TipoServicioData) => {
            const appTipoServicio = new AppParametro();
            appTipoServicio.id_parametro = TipoServicioData.id_parametro;
            appTipoServicio.valor = TipoServicioData.valor;
            appTipoServicio.total = dto.total;
            
            return appTipoServicio;
        });

        return appTipoServicio;
    }
}