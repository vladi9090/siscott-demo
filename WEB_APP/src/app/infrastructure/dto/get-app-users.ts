import { AppUser } from "src/app/domain/models/app-user";
import { User } from "src/app/domain/models/user";

export class GetAppUsersDto {
    total: number;
    data: Array<GetAppUsersDataDto>;

    constructor() { };
}

export class GetAppUsersDataDto {
    idUsuario: number;
    nombre: string;
    apellidos: string;
    dni: string;
    email: string;
    NomUsuario: string;
    password: string;
    Telefono: string;
    firebaseToken?: string;
    constructor() { };
}

export class GetUsersDtoTransformer {

    constructor() { }

    transform(dto: GetAppUsersDto): Array<AppUser> {

        var appUsers = dto.data.map((userData) => {
            const appUser = new AppUser();
            appUser.idUsuario = userData.idUsuario;
            appUser.firebaseToken = userData.firebaseToken;
            appUser.nombre = userData.nombre;
            appUser.apellidos = userData.apellidos;
            appUser.email = userData.email;
            appUser.password = userData.password;
            appUser.dni = userData.dni;
            appUser.total = dto.total;
            return appUser;
        });

        return appUsers;
    }
}