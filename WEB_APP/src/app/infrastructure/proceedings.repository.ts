import { Injectable } from "@angular/core";
import { AppResolucionAdministrativa } from "@app/domain/models/app-resolution";
import { ProceedingsRepository } from "@app/domain/repositories/proceedings.repository";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AppUser } from "../domain/models/app-user";
import { GetAppResolucionesAdministradoDto, GetResolucionesAdministradoDtoTransformer } from "./dto/get-app-resolutions";
import { GetAppUsersDto, GetUsersDtoTransformer } from "./dto/get-app-users";
import { ApiService } from "./remote/api.service";
import { AppGrupoFormato } from '../domain/models/app-format-group';
import { GetAppGrupoFormatoDto, GetGrupoFormatoDtoTransformer } from "./dto/get-app-format-group";
import { AppTipoFormato } from "@app/domain/models/app-type-format";
import { GetAppTipoFormatoDto, GetTipoFormatoDtoTransformer } from "./dto/get-app-type-format";
import { AppParametro } from '@app/domain/models/app-parameter';
import { GetAppParametroDto, GetTipoResolucionDtoTransformer, GetTipoServicioDtoTransformer } from "./dto/get-app-parameter";
import { GetAppGrupoResolutorDto, GetGrupoResolutorDtoTransformer } from "./dto/get-app-group-resolutor";
import { AppGrupoResolutor } from '../domain/models/app-group-resolutor';

@Injectable()
export class ProceedingsRepositoryImpl implements ProceedingsRepository {

    constructor(
        private apiService: ApiService, 
    ) { }

    getAppUsers(name?: string, dni?: string): Observable<AppUser[]>{
        return this.apiService
            .get(this.apiService.appUsers, {name: name, dni: dni})
            .pipe(
                map((dto: GetAppUsersDto) =>
                    new GetUsersDtoTransformer().transform(dto)),
            );
    }

    getAppResolucionAdministrativa(GrupoFormato?: number, TipoResolucion?: number, TipoServicio?: number, TipoFalta?: number, GrupoResolutor?: number, NumeroActas?: string, Administrado?: string, NumeroExpediente?: string): Observable<AppResolucionAdministrativa[]>{
        return this.apiService
            .post(this.apiService.appResolution, {grupo_formato: GrupoFormato, tipo_resolucion: TipoResolucion, TipoServicio: TipoServicio, tipo_falta: TipoFalta, grupo_resolutor: GrupoResolutor, numero_actas: NumeroActas, administrado: Administrado, numero_expediente: NumeroExpediente})
            .pipe(
                map((dto: GetAppResolucionesAdministradoDto) =>
                    new GetResolucionesAdministradoDtoTransformer().transform(dto)),
            );
    }

    getAppGrupoFormato() : Observable<AppGrupoFormato[]>{
        return this.apiService
            .get(this.apiService.appGrupoFormato, {})
            .pipe(
                map((dto: GetAppGrupoFormatoDto) =>
                    new GetGrupoFormatoDtoTransformer().transform(dto)),
            );
    }

    getAppTipoFormato(IdGrupoFormato: number) : Observable<AppTipoFormato[]>{
        return this.apiService
            .get(this.apiService.appTipoFormato + "/" + IdGrupoFormato)
            .pipe(
                map((dto: GetAppTipoFormatoDto) =>
                    new GetTipoFormatoDtoTransformer().transform(dto)),
            );
    }

    getAppTipoServicio(IdTipoFormato: number) : Observable<AppParametro[]>{
        return this.apiService
            .get(this.apiService.appTipoServicio + "/" + IdTipoFormato)
            .pipe(
                map((dto: GetAppParametroDto) =>
                    new GetTipoServicioDtoTransformer().transform(dto)),
            );
    }

    getAppTipoResolucion() : Observable<AppParametro[]>{
        return this.apiService
            .get(this.apiService.getTipoResolucion, {})
            .pipe(
                map((dto: GetAppParametroDto) =>
                    new GetTipoResolucionDtoTransformer().transform(dto)),
            );
    }

    getAppGrupoResolutor() : Observable<AppGrupoResolutor[]>{
        return this.apiService
            .get(this.apiService.getGrupoResolutor, {})
            .pipe(
                map((dto: GetAppGrupoResolutorDto) =>
                    new GetGrupoResolutorDtoTransformer().transform(dto)),
            );
    }
    
}