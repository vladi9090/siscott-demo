import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from "rxjs";
import { environment } from "src/environments/environment";
import { catchError } from 'rxjs/operators';
import { Failure } from "src/app/domain/error/failure";
import { Formatter } from "../utils/formatter";


@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }

  //login = environment.api + '/user/login';
  login = environment.api + 'usuario/Login';
  userPermission = environment.api + '/user/permission';
  //apiExpediente = `${environment.api}/`

  appUsers = 'https://2faa8439-a656-45cd-8f61-54b38722c139.mock.pstmn.io/users';
  appResolution = environment.apiResolution + 'DocumentoControl/getActaResolucion';
  generarResolution = environment.apiResolution + 'resolucion/regResolucion';
  appGrupoFormato = environment.apiResolution + 'parametro/getGrupoFormato';
  appTipoFormato = environment.apiResolution + 'parametro/getTipoFormato';
  appTipoServicio = environment.apiResolution + 'parametro/getTipoServicio';
  getTipoResolucion = environment.apiResolution + 'Parametro/getTipoResolucion';
  getTipoPlantillaResolucion = environment.apiResolution + 'Parametro/getTipoPlantillaResolucion';
  getGrupoResolutor = environment.apiResolution + 'Parametro/getGrupoResolutor';
  
  
  post(path: string, body = {}): Observable<any> {
    return this.http.post(path, body).pipe(
      catchError(error => {
        return Formatter.httpErrorFormatter(error);
      })
    );
  }

  get(path: string, params?: any): Observable<any> {
    return this.http.get(path, { params }).pipe(
      catchError(error => {
        return Formatter.httpErrorFormatter(error);
      })
    );
  }

  put(path: string, body = {}): Observable<any> {
    return this.http.put(path, JSON.stringify(body)).pipe(
      catchError(error => {
        return Formatter.httpErrorFormatter(error);
      })
    );
  }

  patch(path: string, body = {}): Observable<any> {
    return this.http.patch(path, JSON.stringify(body)).pipe(
      catchError(error => {
        return Formatter.httpErrorFormatter(error);
      })
    );
  }

  delete(path: string): Observable<any> {
    return this.http.delete(path).pipe(
      catchError(error => {
        return Formatter.httpErrorFormatter(error);
      })
    );
  }

}
