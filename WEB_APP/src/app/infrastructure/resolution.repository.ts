import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ResolutionRepository } from '../domain/repositories/resolution.repository';
import { ResolutionCommand } from '../application/commands/resolution.command';
import { AppRequest } from '../domain/models/app-request';
import { ApiService } from './remote/api.service';
import { map } from 'rxjs/operators';
import { RequestDto } from './dto/get-data-request';
import { GetParametro } from './dto/get-app-parametro';



@Injectable()
export class ResolutionRepositoryImpl implements ResolutionRepository {

    constructor(
        private apiService: ApiService,
    ) {

    }

    postGenerarResolucion(command: ResolutionCommand): Observable<AppRequest> {
        return this.apiService
            .post(this.apiService.generarResolution, command)
            .pipe(
                map((dto: RequestDto) => {
                    return { ...dto }
                })
            )
    }

    getTipoResolucion(): Observable<any> {
        return this.apiService
            .get(this.apiService.getTipoResolucion)
            .pipe(
                map((dto: any) => {
                    return { ...dto }
                })
            )
    }

    getTipoPlantillaResolucion(codigo: number): Observable<any> {
        return this.apiService
            .get(this.apiService.getTipoPlantillaResolucion + '/' + codigo)
            .pipe(
                map((dto: any) => {
                    return { ...dto }
                })
            )
    }

}