
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { AppUser } from "../domain/models/app-user";
import { User } from "../domain/models/user";

import { UserRepository } from "../domain/repositories/user.repository";
import { GetAppUsersDto, GetUsersDtoTransformer } from "./dto/get-app-users";
import { UserLoginDto, UserLoginDtoTransformer } from "./dto/user-login.dto";
import { BrowserStorage } from "./local/browser.storage";
import { ApiService } from "./remote/api.service";
import { DataRequestDto } from './dto/get-data-request';

@Injectable()
export class UserRepositoryImpl implements UserRepository {

    constructor(
        private apiService: ApiService,
        private browserStorage: BrowserStorage
    ) { }

    loginUserCustom(email: string, password: string): Observable<DataRequestDto<User>> {
        const params = {
            NomUsuario: email,
            Password: password
        }
        return this.apiService
            .post(this.apiService.login, params)
            .pipe(
                map((dto: DataRequestDto<UserLoginDto>) => {
                    //new UserLoginDtoTransformer().transform(dto.data)
                    // return {
                    //     ...dto
                    // }
                    return {
                        ...dto,
                        data: dto.code > 0 ? new UserLoginDtoTransformer().transform(dto.data) : new User()
                    }

                }));
    }

    loginUser(email: string, password: string): Observable<User> {
        const params = {
            email: email,
            password: password
        }
        return this.apiService
            .post(this.apiService.login, params)
            .pipe(
                map((dto: UserLoginDto) =>
                    new UserLoginDtoTransformer().transform(dto)
                ),
            );
    }

    saveUserSession(user: User) {
        this.browserStorage.saveItem('id', user.idUsuario.toString());
        this.browserStorage.saveItem('accessToken', user.accessToken || '');
    }

    getUserSession(): User {
        const user = new User();
        user.idUsuario = this.browserStorage.getItem('id') ? Number(this.browserStorage.getItem('id')) : 0;
        user.accessToken = this.browserStorage.getItem('accessToken')!.toString();
        return user;
    }

    getAppUsers(name: string, dni: string): Observable<AppUser[]> {
        return this.apiService
            .get(this.apiService.appUsers)
            .pipe(
                map((dto: GetAppUsersDto) =>
                    new GetUsersDtoTransformer().transform(dto)),
            );
    }

}