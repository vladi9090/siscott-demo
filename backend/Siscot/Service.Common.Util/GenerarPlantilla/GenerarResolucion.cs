﻿using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Text;
using Expediente.Service.Queries.DTOs;
using System.IO;
using System.Configuration;
using Spire.Doc;
using System.Threading.Tasks;
using Expediente.Domain;
using Expediente.Persistence.Database;
using System.Linq;

namespace Service.Common.Util.GenerarPlantilla
{
    public class GenerarResolucion
    {
        //private readonly ApplicationDbContext _context;
        public BEFileResponse GenerarResolucionPDF(ActaResolucionMasivaDto getResolucion)
        {
            var resp = new DataResponse();
            var Fileresp = new BEFileResponse();

            try
            {
                var pathSaveVirtual = "h:/root/home/bvelasquez-001/www/appdemo/archivos/";
                var pathSave = pathSaveVirtual;
                var pathSaveVirtualPDF = pathSaveVirtual;
                var pathSavePDF = pathSaveVirtualPDF;

                int ModeloPlantilla = getResolucion.id_tipo_resolucion;
                string PlantillaDocx = "";
                switch (ModeloPlantilla)
                {
                    case 1: //Caducidad
                        PlantillaDocx = pathSaveVirtual + "Caducidad.docx";
                        break;
                    case 2: //Inicio
                        PlantillaDocx = pathSaveVirtual + "Inicio.docx";
                        break;
                    case 3: //Reconocimiento Pago
                        PlantillaDocx = pathSaveVirtual + "ReconocimientoPago.docx";
                        break;

                }
                if (File.Exists(PlantillaDocx))
                {
                    //string genIdFile = Guid.NewGuid().ToString("N");
                    //string sNameGenPdf = genIdFile + ".pdf";
                    string sNameGenPdf = getResolucion.numero_resolucion.Replace("/", "-") + ".pdf";
                    pathSaveVirtual = pathSaveVirtualPDF + sNameGenPdf;
                    pathSavePDF = pathSavePDF + sNameGenPdf;
                    resp.Message = pathSaveVirtual;

                    switch (ModeloPlantilla)
                    {
                        case 1: //Caducidad
                            ResolucionCaducidad(PlantillaDocx, pathSavePDF, getResolucion);
                            break;
                        case 2: //Inicio
                            ResolucionInicio(PlantillaDocx, pathSavePDF, getResolucion);
                            break;
                        case 3: //Reconocimiento Pago
                            ResolucionReconocimientoPago(PlantillaDocx, pathSavePDF, getResolucion);
                            break;
                    }

                    Fileresp.FileName = sNameGenPdf;
                    Fileresp.FileRuta = pathSaveVirtualPDF;
                    var respDoc = new DataResponse();
                    //respDoc = RegDocumentoExterno(getResolucion,Fileresp);
                    resp.Status = true;
                }
                else
                {
                    resp.Status = false;
                    resp.Message = "No Existe Plantilla Principal o Ruta";
                }
            }
            catch (Exception ex)
            {
                resp.Status = false;
                resp.Message = "Error Inesperado: " + ex.Message;
            }

            return Fileresp;
        }

        protected DataResponse ResolucionCaducidad(string RutaPlantilla, string RutaSave, ActaResolucionMasivaDto ent)
        {
            var resp = new DataResponse();
            try
            {
                Document doc = new Document();
                doc.LoadFromFile(RutaPlantilla, FileFormat.Auto);
                doc.Replace("«N_DE_RESOLUCIÓN»", ent.numero_resolucion, true, true);
                doc.Replace("«FECHA_DE_RESOLUCIÓN»", ent.fecha_resolucion.Value.ToString("dd") + " de " + ent.fecha_resolucion.Value.ToString("MMMM") + " del " + ent.fecha_resolucion.Value.ToString("yyyy"), true, true);
                doc.Replace("«N_PAPELETA»", ent.numero_acta.ToString(), true, true);
                doc.Replace("«FECHA_PAPELETA»", ent.fecha_acta.Value.ToString("dd") + " de " + ent.fecha_acta.Value.ToString("MMMM") + " del " + ent.fecha_acta.Value.ToString("yyyy"), true, true);
                doc.Replace("«N_EXPEDIENTE»", ent.numero_expediente, true, true);
                doc.Replace("«ADMINISTRADO»", ent.administrado_resolucion, true, true);
                doc.Replace("«CODIGO_FALTA»", ent.codigo_falta, true, true);
                doc.Replace("«RESOLUTOR»", ent.usu_reg_resolucion, true, true);

                doc.SaveToFile(RutaSave, FileFormat.PDF);
                doc.Close();
                resp.Status = true;
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
            }
            return resp;
        }

        protected DataResponse ResolucionInicio(string RutaPlantilla, string RutaSave, ActaResolucionMasivaDto ent)
        {
            var resp = new DataResponse();
            try
            {
                Document doc = new Document();
                doc.LoadFromFile(RutaPlantilla, FileFormat.Auto);
                doc.Replace("«N_DE_RESOLUCIÓN»", ent.numero_resolucion, true, true);
                doc.Replace("«FECHA_DE_RESOLUCIÓN»", ent.fecha_resolucion.Value.ToString("dd") + " de " + ent.fecha_resolucion.Value.ToString("MMMM") + " del " + ent.fecha_resolucion.Value.ToString("yyyy"), true, true);
                doc.Replace("«N_PAPELETA»", ent.numero_acta.ToString(), true, true);
                doc.Replace("«FECHA_PAPELETA»", ent.fecha_acta.Value.ToString("dd") + " de " + ent.fecha_acta.Value.ToString("MMMM") + " del " + ent.fecha_acta.Value.ToString("yyyy"), true, true);
                doc.Replace("«N_EXPEDIENTE»", ent.numero_expediente, true, true);
                doc.Replace("«ADMINISTRADO»", ent.administrado_resolucion, true, true);
                doc.Replace("«CODIGO_FALTA»", ent.codigo_falta, true, true);
                doc.Replace("«RESOLUTOR»", ent.usu_reg_resolucion, true, true);

                doc.SaveToFile(RutaSave, FileFormat.PDF);
                doc.Close();
                resp.Status = true;
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

        protected DataResponse ResolucionReconocimientoPago(string RutaPlantilla, string RutaSave, ActaResolucionMasivaDto ent)
        {
            var resp = new DataResponse();
            try
            {
                Document doc = new Document();
                doc.LoadFromFile(RutaPlantilla, FileFormat.Auto);
                doc.Replace("«N_DE_RESOLUCIÓN»", ent.numero_resolucion, true, true);
                doc.Replace("«FECHA_DE_RESOLUCIÓN»", ent.fecha_resolucion.Value.ToString("dd") + " de " + ent.fecha_resolucion.Value.ToString("MMMM") + " del " + ent.fecha_resolucion.Value.ToString("yyyy"), true, true);
                doc.Replace("«N_PAPELETA»", ent.numero_acta.ToString(), true, true);
                doc.Replace("«FECHA_PAPELETA»", ent.fecha_acta.Value.ToString("dd") + " de " + ent.fecha_acta.Value.ToString("MMMM") + " del " + ent.fecha_acta.Value.ToString("yyyy"), true, true);
                doc.Replace("«N_EXPEDIENTE»", ent.numero_expediente, true, true);
                doc.Replace("«ADMINISTRADO»", ent.administrado_resolucion, true, true);
                doc.Replace("«CODIGO_FALTA»", ent.codigo_falta, true, true);
                doc.Replace("«RESOLUTOR»", ent.usu_reg_resolucion, true, true);

                doc.SaveToFile(RutaSave, FileFormat.PDF);
                doc.Close();
                resp.Status = true;
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
            }
            return resp;
        }
    }
}
