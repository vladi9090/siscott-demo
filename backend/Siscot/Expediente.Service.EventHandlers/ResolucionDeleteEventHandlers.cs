﻿using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class ResolucionDeleteEventHandlers : INotificationHandler<ResolucionDeleteCommand>
    {
        private readonly ApplicationDbContext _context;

        public ResolucionDeleteEventHandlers(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ResolucionDeleteCommand command, CancellationToken cancellationToken)
        {
            var resolucion = _context.Resolucion_Administrativa.Single(x => x.id_resolucion == command.id_resolucion);

            _context.Remove(resolucion);

            await _context.SaveChangesAsync();

        }
    }
}
