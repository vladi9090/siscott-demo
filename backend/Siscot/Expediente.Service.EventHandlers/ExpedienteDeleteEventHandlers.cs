﻿using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class ExpedienteDeleteEventHandlers : INotificationHandler<ExpedienteDeleteCommand>
    {
        private readonly ApplicationDbContext _context;

        public ExpedienteDeleteEventHandlers(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ExpedienteDeleteCommand command, CancellationToken cancellationToken)
        {

            //
            await _context.SaveChangesAsync();

        }
    }
}
