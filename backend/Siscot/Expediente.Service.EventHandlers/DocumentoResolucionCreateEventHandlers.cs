﻿using Expediente.Domain;
using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class DocumentoResolucionCreateEventHandlers : IRequestHandler<DocumentoResolucionCreateCommand, DataResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ResolucionCreateEventHandlers> _logger;
        public DocumentoResolucionCreateEventHandlers(ApplicationDbContext context, ILogger<ResolucionCreateEventHandlers> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<DataResponse> Handle(DocumentoResolucionCreateCommand command, CancellationToken cancellationToken)
        {
            var resp = new DataResponse();
            try
            {
                _logger.LogInformation("--- Insertando el documento externo");

                await _context.AddAsync(new Documento_Externo
                {
                    id_tipo_documento = 1,
                    extension = ".pdf",
                    nombre_archivo = command.nombre_archivo,
                    ruta_archivo = command.ruta_archivo,
                    nombre_fisico = command.nombre_fisico,
                    fecha_reg = DateTime.Now,
                    id_usuario_reg = command.id_usuario_reg,
                    estado = "A"
                });

                await _context.SaveChangesAsync();

                _logger.LogInformation("--- Insertando resolucion adjunto");

                var id_doc_externo = _context.Documento_Externo.
                               Where(x => x.ruta_archivo == command.ruta_archivo && x.nombre_fisico == command.nombre_fisico).
                               Select(x => x.id_documento_externo).FirstOrDefault();

                await _context.AddAsync(new Resolucion_Adjunto
                {
                    id_resolucion = command.id_resolucion,
                    id_documento_externo = id_doc_externo,
                    estado = "A"
                });

                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                resp.Status = false;
                await _context.Database.RollbackTransactionAsync();
            }

            return resp;

        }


    }
}
