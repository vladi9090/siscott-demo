﻿using Expediente.Domain;
using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class ExpedienteUpdateEventHandlers : INotificationHandler<ExpedienteUpdateCommand>
    {
        private readonly ApplicationDbContext _context;

        public ExpedienteUpdateEventHandlers(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ExpedienteUpdateCommand command, CancellationToken cancellation)
        {
           //
            await _context.SaveChangesAsync();
        }

    }
}
