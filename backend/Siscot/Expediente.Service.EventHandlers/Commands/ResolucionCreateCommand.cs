﻿using Expediente.Domain;
using MediatR;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.EventHandlers.Commands
{
    public class ResolucionCreateCommand : IRequest<DataResponse>
    {
        public string numero_resolucion { get; set; }
        public int id_grupo_resolutor { get; set; }
        public int id_dependencia { get; set; }
        public string id_usuario_reg { get; set; }
        public string estado { get; set; }
        public int id_tipo_resolucion { get; set; }
        public int secuencia_detalle { get; set; }
        public int id_documento_control { get; set; }
        public int id_entidad { get; set; }
        public int id_falta { get; set; }
        public int id_expediente{ get; set; }
        public Int64 id_resolucion { get; set; }
        public string nombre_archivo { get; set; }
        public string ruta_archivo { get; set; }
        public string nombre_fisico { get; set; }
        public int id_tipo_plantilla { get; set; }
        public string tipo_plantilla_resolucion { get; set; }
    }
}
