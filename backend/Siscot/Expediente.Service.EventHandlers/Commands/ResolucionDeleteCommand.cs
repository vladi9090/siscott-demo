﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.EventHandlers.Commands
{
    public class ResolucionDeleteCommand : INotification
    {
        public int id_resolucion { get; set; }
    }
}
