﻿using Expediente.Domain;
using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using Microsoft.Extensions.Logging;
using Moq;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class ResolucionCreateEventHandlers : IRequestHandler<ResolucionCreateCommand, DataResponse>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ResolucionCreateEventHandlers> _logger;
        public ResolucionCreateEventHandlers(ApplicationDbContext context, ILogger<ResolucionCreateEventHandlers> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<DataResponse> Handle(ResolucionCreateCommand command, CancellationToken cancellationToken)
        {
            DataResponse resp = new DataResponse();
            try
            {
                _logger.LogInformation("--- ResolucionCreateEventHandlers inicio");

                string numeroResolucion = string.Empty;
                int numeroResol = 1;
                var anio = DateTime.Today.Year;
                string codigo = string.Empty;

                var sufijo = (from u in _context.Dependencia.Where(x => x.id_dependencia == command.id_dependencia)
                              .OrderByDescending(x => x.id_dependencia)
                              select new { codigo = u.codigo, sufijo = u.sufijo }).SingleOrDefault();

                if (sufijo != null)
                {
                    codigo = sufijo.sufijo + sufijo.codigo;
                }
                else
                {
                    resp.Message = "No se encontro el codigo de su dependencia";
                }

                var numero = _context.Resolucion_Administrativa.
                               Where(x => x.id_tipo_resolucion == command.id_tipo_resolucion).
                               Select(x => x.numero_resolucion.Substring(0, 6)).Max();

                if (numero != null)  numeroResol = int.Parse(numero) + 1;

                _logger.LogInformation("--- consultando la base de datos para la generación del nro de resolución");

                numeroResolucion = numeroResol.ToString().PadLeft(6, '0') + "-S-" + anio.ToString() + codigo;

                await _context.AddAsync(new Resolucion_Administrativa
                {
                    numero_resolucion = numeroResolucion,
                    id_grupo_resolutor = command.id_grupo_resolutor,
                    id_dependencia = command.id_dependencia,
                    fecha_reg = DateTime.Now,
                    id_usuario_reg = command.id_usuario_reg,
                    estado = "A",
                    id_tipo_resolucion = command.id_tipo_resolucion,
                    id_tipo_plantilla = command.id_tipo_plantilla
                });

                await _context.SaveChangesAsync();

                var idResolucion = _context.Resolucion_Administrativa.
                               Where(x => x.numero_resolucion == numeroResolucion && x.id_tipo_resolucion == command.id_tipo_resolucion).
                               Select(x => x.id_resolucion).FirstOrDefault();

                await _context.AddAsync(new Resolucion_Administrativa_Detalle
                {
                    id_resolucion = idResolucion,
                    id_documento_control = command.id_documento_control,
                    secuencia_detalle = command.secuencia_detalle,
                    id_falta = command.id_falta,
                    fecha_reg = DateTime.Now,
                    id_usuario_reg = command.id_usuario_reg,
                    estado = "A"
                });

                await _context.SaveChangesAsync();

                await _context.AddAsync(new Resolucion_Administrado
                {
                    id_resolucion = idResolucion,
                    id_entidad = command.id_entidad,
                    fecha_reg = DateTime.Now,
                    id_usuario_reg = command.id_usuario_reg,
                    estado = command.estado
                });

                await _context.SaveChangesAsync();
                resp.IDbdGenerado = idResolucion;
                resp.Code = DataResponse.STATUS_CREADO;
                resp.Message = numeroResolucion;

                _logger.LogInformation("--- ResolucionCreateEventHandlers fin");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                resp.Code = DataResponse.STATUS_ERROR;
                await _context.Database.RollbackTransactionAsync();
                resp.Message = ex.Message;
            }

            return resp;
        }


        
    }

}
