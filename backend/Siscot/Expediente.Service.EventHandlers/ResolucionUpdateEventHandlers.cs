﻿using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class ResolucionUpdateEventHandlers : INotificationHandler<ResolucionUpdateCommand>
    {
        private readonly ApplicationDbContext _context;

        public ResolucionUpdateEventHandlers(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ResolucionUpdateCommand command, CancellationToken cancellation)
        {
            var resolucion = _context.Resolucion_Administrativa.Single(x => x.id_resolucion == command.id_resolucion);
            resolucion.id_grupo_resolutor = command.id_grupo_resolutor;
            resolucion.id_dependencia = command.id_dependencia;
            resolucion.fecha_reg = command.fecha_reg;
            resolucion.id_usuario_reg = command.id_usuario_reg;
            resolucion.fecha_mod = command.fecha_mod;
            resolucion.id_usuario_mod = command.id_usuario_mod;
            resolucion.estado = command.estado;
            resolucion.id_tipo_resolucion = command.id_tipo_resolucion;

            await _context.SaveChangesAsync();
        }

    }
}
