﻿using Expediente.Domain;
using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Expediente.Service.EventHandlers
{
    public class ExpedienteCreateEventHandlers : INotificationHandler<ExpedienteCreateCommand>
    {
        private readonly ApplicationDbContext _context;

        public ExpedienteCreateEventHandlers(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ExpedienteCreateCommand command, CancellationToken cancellation)
        {
            //
            await _context.SaveChangesAsync();
        }
    }
}
