﻿
using Expediente.Domain;
using Microsoft.EntityFrameworkCore;

namespace Expediente.Persistence.Database
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(
         DbContextOptions<ApplicationDbContext> options
         ) : base(options)
        {

        }

        public DbSet<Resolucion_Administrativa> Resolucion_Administrativa { get; set; }
        public DbSet<Dependencia> Dependencia { get; set; }
        public DbSet<Parametro> Parametro { get; set; }
        public DbSet<Parametro_Grupo> Parametro_Grupo { get; set; }
        public DbSet<Documento_Control> Documento_Control { get; set; }
        public DbSet<Entidad> Entidad { get; set; }
        public DbSet<Falta> Falta { get; set; }
        public DbSet<Resolucion_Administrado> Resolucion_Administrado { get; set; }
        public DbSet<Documento_Control_Detalle> Documento_Control_Detalle { get; set; }
        public DbSet<Resolucion_Administrativa_Detalle> Resolucion_Administrativa_Detalle { get; set; }
        public DbSet<Expediente_Administrativo> Expediente_Administrativo { get; set; }
        public DbSet<Expediente_Administrativo_Detalle> Expediente_Administrativo_Detalle { get; set; }

        public DbSet<Grupo_Resolutor> Grupo_Resolutor { get; set; }
        public DbSet<Tipo_Formato> Tipo_Formato { get; set; }
        public DbSet<Grupo_Formato> Grupo_Formato { get; set; }
        public DbSet<Documento_Externo> Documento_Externo { get; set; }
        public DbSet<Resolucion_Adjunto> Resolucion_Adjunto { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //esquema de la bd
            builder.HasDefaultSchema("dbo");
            //builder.Entity<Resolucion_Adjunto>().HasNoKey();

        }

    }
}
