﻿
using System;
using System.Threading.Tasks;
using Service.Common.Collection;

using System.Linq;
using Service.Common.Mapping;
using Service.Common.Paging;
using Usuario.Service.Queries.DTOs;
using Seguridad.Persistence.Database;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Usuario.Service.Queries
{

    public interface IUsuarioQueryService
    {
        Task<DataCollection<UsuarioDto>> GetListarUsuario(int page, int take);

        Task<UsuarioDto> GetUsuarioLogin(string usuario, string password);

        ///Task<UsuarioDto> GetAsync(Int64 Id);
    }

    public class UsuarioQueryService : IUsuarioQueryService
    {
        private readonly ApplicationDbContext _context;

        public UsuarioQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DataCollection<UsuarioDto>> GetListarUsuario(int page, int take)
        {

            var collection = await (from c in _context.Usuario
                                    select new
                                    {
                                        IdUsuario = c.IdUsuario,
                                        Nombre = c.Nombre,
                                        Apellidos = c.Apellidos,
                                        Telefono = c.Telefono,
                                        NomUsuario = c.NomUsuario,
                                        Email = c.Email,
                                        Dni = c.Dni
                                    })
                  .GetPagedAsync(page, take);
            return collection.MapTo<DataCollection<UsuarioDto>>();

        }

        public async Task<UsuarioDto> GetUsuarioLogin(string usuario, string password)
        {

            var collection = await _context.Usuario
                .Where(x => x.NomUsuario.Equals(usuario) && x.Password.Equals(password))
                .SingleOrDefaultAsync();

            //var collection = await (from c in _context.Usuario
            //                        where c.NomUsuario.Equals(usuario) && c.Password.Equals(password)
            //                        select new
            //                        {
            //                            IdUsuario = c.IdUsuario,
            //                            Nombre = c.Nombre,
            //                            Apellidos = c.Apellidos,
            //                            Telefono = c.Telefono,
            //                            NomUsuario = c.NomUsuario,
            //                            Email = c.Email,
            //                            Dni = c.Dni
            //                        })
            //                        .SingleAsync();

            //var data = collection.MapTo<UsuarioDto>();
            //data.Password = "";

            return collection.MapTo<UsuarioDto>();

        }



    }
}
