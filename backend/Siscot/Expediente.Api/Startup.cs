using Expediente.Persistence.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MediatR;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.OpenApi.Models;
using Expediente.Service.Queries;

namespace Expediente.Api
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        private const string DefaultCorsPolicyName = "AllowOrigin";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowOrigin",
            //    builder =>
            //    {
            //        builder.WithOrigins("http://localhost:4200", "http://vladid732-001-site1.itempurl.com")
            //        .AllowAnyHeader()
            //        .AllowAnyMethod();
            //    });
            //});

            //services.AddCors(options =>
            //{

            //    //options.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());

            //    options.AddPolicy("AllowOrigin",
            //    builder => builder.AllowAnyOrigin()
            //    .AllowAnyHeader()
            //    .AllowAnyMethod());
            //    //{
            //});


            services.AddCors(
                options =>
                {
                    options.AddPolicy("CrossPolicy", config =>
                    {
                        config.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                    });
                }
                );


            string conecction = Configuration.GetSection("ConnectionStrings").GetSection("DefaultConecction").Value; //Configuration.GetConnectionString("DefaultConecction");


            services.AddDbContext<ApplicationDbContext>(opts =>
            opts.UseSqlServer(conecction));

            services.AddMediatR(Assembly.Load("Expediente.Service.EventHandlers"));

            services.AddTransient<IResolucionQueryService, ResolucionQueryService>();
            services.AddTransient<IDependenciaQueryService, DependenciaQueryService>();
            services.AddTransient<IActaResolucionMasivaQueryService, ActaResolucionMasivaQueryService>();
            services.AddTransient<IParametroQueryService, ParametroQueryService>();
            services.AddTransient<IDocumentoExternoQueryService, DocumentoExternoQueryService>();
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Manageds.Api", Version = "v1" });
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Manageds.Api v1"));
            }

            app.UseRouting();
            //app.UseCors(DefaultCorsPolicyName); //Enable CORS!
            app.UseCors("CrossPolicy");

            ////app.UseCors(options => options.WithOrigins("*"));
            //app.UseCors(x => x
            //.AllowAnyMethod()
            //.AllowAnyHeader()
            //.SetIsOriginAllowed(origin => true) // allow any origin
            //.AllowCredentials()); // allow credentials



            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // setup app's root folders
            AppDomain.CurrentDomain.SetData("ContentRootPath", env.ContentRootPath);
            AppDomain.CurrentDomain.SetData("WebRootPath", env.WebRootPath);
        }
    }
}
