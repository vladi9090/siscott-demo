﻿using Expediente.Service.Queries;
using Expediente.Service.Queries.DTOs;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expediente.Api.Controllers
{
    [Route("api/DocumentoControl")]
    [ApiController]
    [EnableCors("CrossPolicy")]
    public class DocumentoControlController : ControllerBase
    {
        private readonly IActaResolucionMasivaQueryService _actaResolMasivaQueryService;
        private readonly ILogger<DocumentoControlController> _logger;
        private readonly IMediator _mediator;

        public DocumentoControlController(ILogger<DocumentoControlController> logger,
            IActaResolucionMasivaQueryService actaResolucion,
            IMediator mediator)
        {

            _logger = logger;
            _actaResolMasivaQueryService = actaResolucion;
            _mediator = mediator;

        }

        [HttpPost("getActaResolucion")]
        public async Task<DataCollection<ActaResolucionMasivaDto>> getActaResolucion(RequestActaResolucionDto request)
        {
            var resp = new DataCollection<ActaResolucionMasivaDto>();
            try
            {
                request.page = request.page == 0 ? 1 : request.page;
                request.take = request.take == 0 ? 10 : request.take;

                resp = await _actaResolMasivaQueryService.GetActaResolucion(request);


            }
            catch (Exception ex)
            {

                throw ex;
            }
            return resp;
        }



    }
}
