﻿using Expediente.Service.Queries;
using Expediente.Service.Queries.DTOs;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Expediente.Api.Controllers
{
    [Route("api/Parametro")]
    [ApiController]
    [EnableCors("CrossPolicy")]
    public class ParametroController : ControllerBase
    {

        private readonly IParametroQueryService _parametroQueryService;
        private readonly ILogger<ParametroController> _logger;
        private readonly IMediator _mediator;

        public ParametroController(ILogger<ParametroController> logger,
            IParametroQueryService parametroQueryService,
            IMediator mediator)
        {

            _logger = logger;
            _parametroQueryService = parametroQueryService;
            _mediator = mediator;

        }



        [HttpGet("getTipoPlantillaResolucion/{resolucion}")]
        public async Task<DataCollection<ParametroDto>> GetTipoPlantillaResolucion(int resolucion, int page = 1, int take = 10)
        {
            var resp = new DataCollection<ParametroDto>();
            try
            {

                resp = await _parametroQueryService.GetTipoPlantillaResolucion(resolucion, page, take);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getTipoResolucion")]
        public async Task<DataCollection<ParametroDto>> GetTipoResolucion(int page = 1, int take = 10)
        {
            var resp = new DataCollection<ParametroDto>();
            try
            {
                resp = await _parametroQueryService.GetListarTipoResolucion(page, take);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getGrupoResolutor")]
        public async Task<DataCollection<GrupoResolutorDto>> GetGrupoResolutor(int page = 1, int take = 10)
        {
            var resp = new DataCollection<GrupoResolutorDto>();
            try
            {
                resp = await _parametroQueryService.GetListarGrupoResolutor(page, take);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getTipoServicio")]
        public async Task<DataCollection<ParametroDto>> GetTipoServicio(int page = 1, int take = 10)
        {
            var resp = new DataCollection<ParametroDto>();
            try
            {
                resp = await _parametroQueryService.GetListarTipoServicio(page, take);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getTipoFormato")]
        public async Task<DataCollection<TipoFormatoDto>> GetTipoFormato(int page = 1, int take = 10)
        {
            var resp = new DataCollection<TipoFormatoDto>();
            try
            {
                resp = await _parametroQueryService.GetListTipoFormato(page, take);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getGrupoFormato")]
        public async Task<DataCollection<GrupoFormatoDto>> GetGrupoFormato(int page = 1, int take = 10)
        {
            var resp = new DataCollection<GrupoFormatoDto>();
            try
            {
                resp = await _parametroQueryService.GetListGrupoFormato(page, take);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getTipoFormato/{id}")]
        public async Task<DataCollection<TipoFormatoDto>> GetTipoFormatoXGrupo(int page = 1, int take = 10, int id = 0)
        {
            var resp = new DataCollection<TipoFormatoDto>();
            try
            {
                resp = await _parametroQueryService.GetListTipoFormatoXGrupo(page, take, id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }

        [HttpGet("getTipoServicio/{id}")]
        public async Task<DataCollection<ParametroDto>> GetTipoServicioXFormato(int page = 1, int take = 10, int id = 0)
        {
            var resp = new DataCollection<ParametroDto>();
            try
            {
                resp = await _parametroQueryService.GetListTipoServicioXFormato(page, take, id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;

        }


    }
}
