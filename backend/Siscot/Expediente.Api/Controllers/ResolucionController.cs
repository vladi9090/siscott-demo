﻿using Expediente.Service.EventHandlers.Commands;
using Expediente.Service.Queries;
using Expediente.Service.Queries.DTOs;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;
using System;

using System.Threading.Tasks;
using System.IO;

using Expediente.Service.Queries.DTOs;
using Expediente.Service.EventHandlers.Commands;
using Service.Common.Util.GenerarPlantilla;
using Expediente.Domain;
using Expediente.Service.EventHandlers;
using Microsoft.Extensions.Hosting;

namespace Expediente.Api.Controllers
{

    [Route("api/resolucion")]
    [ApiController]
    [EnableCors("CrossPolicy")]
    public class ResolucionController : ControllerBase
    {
        private readonly IResolucionQueryService _resolucionQueryService;
        private readonly ILogger<ResolucionController> _logger;
        private readonly IMediator _mediator;
        private readonly IParametroQueryService _parametroQueryService;
        private readonly IDocumentoExternoQueryService _documentoExternoQueryService;


        public ResolucionController(ILogger<ResolucionController> logger,
            IResolucionQueryService resolucionQueryService,
            IMediator mediator, IParametroQueryService parametroQueryService,
            IDocumentoExternoQueryService documentoExternoQueryService
            )
        {

            _logger = logger;
            _resolucionQueryService = resolucionQueryService;
            _mediator = mediator;
            _parametroQueryService = parametroQueryService;
            _documentoExternoQueryService = documentoExternoQueryService;
        }

        [HttpGet("getDocumentoResolucion")]
        public DataResponse GetArchivoResolucion(int page = 1, int take = 10)
        {
            var resp = new DataResponse();
            try
            {

                //resp = await _resolucionQueryService.GetListarResolucion(page, take);

                //Byte[] bytes = File.ReadAllBytes("path");
                //String file = Convert.ToBase64String(bytes);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }


        [HttpGet("getResolucion")]
        public async Task<DataCollection<ResolucionAdministrativaDto>> GetResolucionMasiva(int page = 1, int take = 10)
        {
            var resp = new DataCollection<ResolucionAdministrativaDto>();
            try
            {
                resp = await _resolucionQueryService.GetListarResolucion(page, take);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

        [HttpPost("regResolucion")]
        public async Task<DataResponse> CrearResolucion(ResolucionCreateCommand command)
        {
            var res = new DataResponse<BEFileResponse>();
            var resp = new DataResponse();
            var request = new ActaResolucionMasivaDto();
            try
            {

                var tipoplantilla = await _parametroQueryService.GetListTipoPlantilla(command.id_tipo_plantilla);
                if (tipoplantilla.sigla == "ESTATICA")
                {
                    resp = await _mediator.Send(command);
                    if (resp.Status)
                    {
                        request.id_tipo_resolucion = command.id_tipo_resolucion;
                        request.id_resolucion = resp.IDbdGenerado;
                        var respuesta = await _resolucionQueryService.GetDatosResolucion(request);
                        GenerarResolucion gen = new GenerarResolucion();
                        res.Data = gen.GenerarResolucionPDF(respuesta);
                        if (res.Status)
                        {
                            var respAdjRes = new DataResponse();
                            DocumentoResolucionCreateCommand objCommand = new DocumentoResolucionCreateCommand();
                            objCommand.id_resolucion = request.id_resolucion;
                            objCommand.id_usuario_reg = request.usu_reg_resolucion;
                            objCommand.nombre_archivo = res.Data.FileName;
                            objCommand.ruta_archivo = res.Data.FileRuta;
                            objCommand.nombre_fisico = res.Data.FileName;
                            respAdjRes = await _mediator.Send(objCommand);
                        }
                    }
                }
                else
                {
                    resp.Message = "No se puede generar resolucion al tipo de plantilla seleccionado";
                    resp.Status = false;
                }
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

        [HttpPut]
        public async Task<DataResponse> UpdateResolucion(ResolucionUpdateCommand command)
        {
            var resp = new DataResponse();
            try
            {
                await _mediator.Publish(command);
                resp.Code = DataResponse.STATUS_CREADO;
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

        public async Task<DataResponse> DeleteResolucion(ResolucionDeleteCommand command)
        {
            var resp = new DataResponse();
            try
            {
                await _mediator.Publish(command);
                resp.Code = DataResponse.STATUS_CREADO;
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

        [HttpPost("GenerarPDF")]
        public async Task<DataResponse<BEFileResponse>> GenerarResolucionPDF(ActaResolucionMasivaDto request)
        {
            var resp = new DataResponse<BEFileResponse>();
            var respData = new ActaResolucionMasivaDto();
            try
            {
                respData = await _resolucionQueryService.GetDatosResolucion(request);
                GenerarResolucion gen = new GenerarResolucion();
                resp.Data = gen.GenerarResolucionPDF(respData);
                if (resp.Status)
                {
                    DocumentoResolucionCreateCommand objCommand = new DocumentoResolucionCreateCommand();
                    objCommand.id_resolucion = request.id_resolucion;
                    objCommand.id_usuario_reg = request.usu_reg_resolucion;
                    objCommand.nombre_archivo = resp.Data.FileName;
                    objCommand.ruta_archivo = resp.Data.FileRuta;
                    objCommand.nombre_fisico = resp.Data.FileName;

                    var respDoc = new DataResponse();
                    
                    await _mediator.Send(objCommand);
                    respDoc.Code = DataResponse.STATUS_CREADO;
                }
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Status = false;
                resp.Code = DataResponse.STATUS_ERROR;
            }

            return resp;
        }

        [HttpGet("imprimirPdf/{id}")]
        public async Task<DocumentoExternoDto> Get(int  id)
        {
            var respData = new DocumentoExternoDto();
            respData =  await _documentoExternoQueryService.GetDocumentoExternoxId(id);
            string path = respData.ruta_archivo + respData.nombre_fisico;
            byte[] b = System.IO.File.ReadAllBytes(path);
            respData.nombre_archivo = "data:image/png;base64," + Convert.ToBase64String(b);
            return respData;
        }

    }

}
