﻿using MediatR;

namespace Seguridad.Service.EventHandlers.Commands
{
    public class UsuarioDeleteCommand: INotification
    {
        public int IdUsuario { get; set; }
 
    }
}
