﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Seguridad.Domain;
using Seguridad.Persistence.Database;
using Seguridad.Service.EventHandlers.Commands;

namespace Usuario.Service.EventHandlers
{
    public class UsuarioEventHandler : INotificationHandler<UsuarioCreateCommand>,
        INotificationHandler<UsuarioDeleteCommand>, INotificationHandler<UsuarioUpdateCommand>

    {
        private readonly ApplicationDbContext _context;

        public UsuarioEventHandler(
             ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(UsuarioCreateCommand command, CancellationToken cancellationToken)
        {
            var findMaxUsuario = (from u in _context.Usuario.OrderByDescending(o => o.IdUsuario)
                                  select new { id = u.IdUsuario}).SingleOrDefault();
            int IdMax = findMaxUsuario.id + 1;

            await _context.AddAsync(new usuario()
            {
                IdUsuario = IdMax,
                Nombre = command.Nombre,
                Apellidos = command.Apellidos,
                NomUsuario = command.NomUsuario,
                Password = command.Password,
                Telefono = command.Telefono,
                Dni = command.Dni,
                Email = command.Email
            });

            await _context.SaveChangesAsync();

        }

        public async Task Handle(UsuarioDeleteCommand command, CancellationToken cancellationToken)
        {
            var usuario = _context.Usuario.Single(x => x.IdUsuario == command.IdUsuario);

            _context.Remove(usuario);

            await _context.SaveChangesAsync();

        }

        public async Task Handle(UsuarioUpdateCommand command, CancellationToken cancellationToken)
        {
            var usuario = _context.Usuario.Single(x => x.IdUsuario == command.IdUsuario);

            usuario.Nombre = command.Nombre;
            usuario.Apellidos = command.Apellidos;
            usuario.NomUsuario = command.NomUsuario;
            usuario.Password = command.Password;
            usuario.Telefono = command.Telefono;
            usuario.Dni = command.Dni;
            usuario.Email = command.Email;
            await _context.SaveChangesAsync();

        }

    }
}
