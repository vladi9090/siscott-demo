﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Seguridad.Domain
{
    public class usuario
    {
        [Key]
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public string NomUsuario { get; set; }

        public string Password { get; set; }

        public string Dni { get; set; }

    }
}
