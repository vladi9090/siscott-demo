﻿using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Seguridad.Service.EventHandlers.Commands;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Usuario.Service.Queries;
using Usuario.Service.Queries.DTOs;

namespace Seguridad.Api.Controlles
{
    [Route("api/usuario")]
    [ApiController]
    [EnableCors("CrossPolicy")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioQueryService _iusuarioQueryService;
        private readonly ILogger<UsuarioController> _logger;
        private readonly IMediator _mediator;

        public UsuarioController(
          ILogger<UsuarioController> logger,
          IUsuarioQueryService usuarioQueryService,
          IMediator mediator
          )
        {
            _logger = logger;
            _iusuarioQueryService = usuarioQueryService;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<DataCollection<UsuarioDto>> GetUsuario(int page = 1, int take = 10)
        
        {
            var resp = new DataCollection<UsuarioDto>();

            try
            {

                resp = await _iusuarioQueryService.GetListarUsuario(page, take);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return resp;


        }

        [HttpPost("Login")]
        public async Task<DataResponse<UsuarioDto>> Login(UsuarioDto usuario)
        {
            var resp = new DataResponse<UsuarioDto>();
            try
            {

                resp.Data = await _iusuarioQueryService.GetUsuarioLogin(usuario.NomUsuario, usuario.Password);
                if (resp.Data != null && resp.Data.IdUsuario > 0)
                {
                    resp.Data.Password = "";
                    resp.Message = "Bienvenido";
                    resp.Code = DataResponse.STATUS_MODIFICADO;
                }
                else
                {
                    resp.Code = DataResponse.STATUS_ERROR;
                    resp.Status = false;
                    resp.Message = "Usuario o Contraseña Incorrectos";
                }
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.Code = DataResponse.STATUS_EXCEPTION;
                resp.Status = false;
                //throw ex;
            }

            return resp;
        }

        [HttpPost("CrearUsuario")]
        public async Task<DataResponse>  CrearUsuario(UsuarioCreateCommand command)
        {
            var resp = new DataResponse();
            try {

                await _mediator.Publish(command);
                resp.Code = DataResponse.STATUS_CREADO;

            } catch (Exception ex) {

                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

        [HttpPut]
        public async Task<DataResponse> UpdateUsuario(UsuarioUpdateCommand command)
        {
            var resp = new DataResponse();
            try
            {
                await _mediator.Publish(command);
                resp.Code = DataResponse.STATUS_CREADO;
            }
            catch (Exception ex)
            {

                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

        [HttpDelete]
        public async Task<DataResponse> DeleteUsuario(UsuarioDeleteCommand command)
        {
            var resp = new DataResponse();

            try
            {
                await _mediator.Publish(command);
                resp.Code = DataResponse.STATUS_CREADO;
            }
            catch (Exception ex)
            {

                resp.Message = ex.Message;
                resp.Status = false;
            }

            return resp;
        }

    }
}
