﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Seguridad.Domain;

namespace Seguridad.Persistence.Database
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options
            ) : base(options)
        {

        }

        public DbSet<usuario> Usuario { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //esquema de la bd
            builder.HasDefaultSchema("dbo");
            ModelConfig(builder);
        }

        public void ModelConfig(ModelBuilder modelBuilder)
        {
            //new ManagedConfiguration(modelBuilder.Entity<Managed>());
        }

    }
}
