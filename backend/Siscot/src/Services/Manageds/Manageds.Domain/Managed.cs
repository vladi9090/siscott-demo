﻿
namespace Manageds.Domain
{
    public class Managed
    {
        public int ManagedId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
    }
}
