﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manageds.Service.Queries.DTOs
{
    public class ManagedDto
    {
        public int ManagedId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
