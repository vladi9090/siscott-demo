﻿using Manageds.Persistence.Database;
using Manageds.Service.Queries.DTOs;
using Microsoft.EntityFrameworkCore;
using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manageds.Service.Queries
{
    public interface IManagedQueryService
    {
        Task<DataCollection<ManagedDto>> GetAllAsync(int page, int take, IEnumerable<int> manageds = null);
        Task<ManagedDto> GetAsync(int Id);
    }
    public class ManagedQueryService : IManagedQueryService
    {
        private readonly ApplicationDbContext _context;
        public ManagedQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DataCollection<ManagedDto>> GetAllAsync(int page, int take, IEnumerable<int> manageds = null)
        {
            var collection = await _context.Managed
                .Where(x => manageds == null || manageds.Contains(x.ManagedId))
                .OrderByDescending(x => x.ManagedId)
                .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<ManagedDto>>();
        }

        public async Task<ManagedDto> GetAsync(int Id)
        {
            return (await _context.Managed.SingleAsync(x => x.ManagedId == Id)).MapTo<ManagedDto>();
        }

    }
}
