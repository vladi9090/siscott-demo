﻿using Manageds.Service.EventHandlers.Commands;
using Manageds.Service.Queries;
using Manageds.Service.Queries.DTOs;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manageds.Api.Controllers
{
    [ApiController]
    [Route("manageds")]
    public class ManagedController : ControllerBase
    {
        private readonly IManagedQueryService _managedQueryService;
        private readonly ILogger<ManagedController> _logger;
        private readonly IMediator _mediator;

        public ManagedController(
            ILogger<ManagedController> logger,
            IManagedQueryService managedQueryService,
            IMediator mediator
            )
        {
            _logger = logger;
            _managedQueryService = managedQueryService;
            _mediator = mediator;
        }

        //manageds
        [HttpGet]
        public async Task<DataCollection<ManagedDto>> GetAll(int page = 1, int take = 10, string ids = null)
        {
            IEnumerable<int> manageds = null;
            if (!string.IsNullOrEmpty(ids))
            {
                manageds = ids.Split(',').Select(x => Convert.ToInt32(x));
            }

            return await _managedQueryService.GetAllAsync(page, take, manageds);
        }

        //manageds/1
        [HttpGet("{id}")]
        public async Task<ManagedDto> GetAll(int id)
        {
            return await _managedQueryService.GetAsync(id);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ManagedCreateCommand command)
        {
            await _mediator.Publish(command);
            return Ok();
        }
    }
}
