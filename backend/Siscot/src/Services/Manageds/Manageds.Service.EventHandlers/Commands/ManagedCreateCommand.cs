﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Manageds.Service.EventHandlers.Commands
{
    public class ManagedCreateCommand: INotification
    {
        public int ManagedId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
