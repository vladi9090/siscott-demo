﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Manageds.Domain;
using Manageds.Persistence.Database;
using Manageds.Service.EventHandlers.Commands;
using MediatR;

namespace Manageds.Service.EventHandlers
{
    public class ManagedCreateEventHandler: 
        INotificationHandler<ManagedCreateCommand>
    {

        private readonly ApplicationDbContext _context;
        public ManagedCreateEventHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ManagedCreateCommand command, CancellationToken cancellationToken)
        {
            await _context.AddAsync(new Managed()
            {
                Name = command.Name,
                LastName =  command.LastName,
                Phone = command.Phone
            });

            await _context.SaveChangesAsync();
        }
    }
}
