﻿
using Manageds.Domain;
using Microsoft.EntityFrameworkCore;
using Manageds.Persistence.Database.Configuration;

namespace Manageds.Persistence.Database
{
    //add-migration initialize
    //update-database

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options
            ) : base(options)
        {

        }

        public DbSet<Managed> Managed { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //esquema de la bd
            builder.HasDefaultSchema("dbo");
            ModelConfig(builder);
        }

        public void ModelConfig(ModelBuilder modelBuilder)
        {
            new ManagedConfiguration(modelBuilder.Entity<Managed>());
        }
    
    }
}
