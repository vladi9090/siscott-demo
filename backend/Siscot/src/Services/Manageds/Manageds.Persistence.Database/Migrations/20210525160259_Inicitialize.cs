﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Manageds.Persistence.Database.Migrations
{
    public partial class Inicitialize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Managed");

            migrationBuilder.CreateTable(
                name: "Managed",
                schema: "Managed",
                columns: table => new
                {
                    ManagedId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Managed", x => x.ManagedId);
                });

            migrationBuilder.InsertData(
                schema: "Managed",
                table: "Managed",
                columns: new[] { "ManagedId", "LastName", "Name", "Phone" },
                values: new object[,]
                {
                    { 1, "Nombre 1", "Nombre 1", "Phone 1" },
                    { 18, "Nombre 18", "Nombre 18", "Phone 18" },
                    { 17, "Nombre 17", "Nombre 17", "Phone 17" },
                    { 16, "Nombre 16", "Nombre 16", "Phone 16" },
                    { 15, "Nombre 15", "Nombre 15", "Phone 15" },
                    { 14, "Nombre 14", "Nombre 14", "Phone 14" },
                    { 13, "Nombre 13", "Nombre 13", "Phone 13" },
                    { 12, "Nombre 12", "Nombre 12", "Phone 12" },
                    { 11, "Nombre 11", "Nombre 11", "Phone 11" },
                    { 10, "Nombre 10", "Nombre 10", "Phone 10" },
                    { 9, "Nombre 9", "Nombre 9", "Phone 9" },
                    { 8, "Nombre 8", "Nombre 8", "Phone 8" },
                    { 7, "Nombre 7", "Nombre 7", "Phone 7" },
                    { 6, "Nombre 6", "Nombre 6", "Phone 6" },
                    { 5, "Nombre 5", "Nombre 5", "Phone 5" },
                    { 4, "Nombre 4", "Nombre 4", "Phone 4" },
                    { 3, "Nombre 3", "Nombre 3", "Phone 3" },
                    { 2, "Nombre 2", "Nombre 2", "Phone 2" },
                    { 19, "Nombre 19", "Nombre 19", "Phone 19" },
                    { 20, "Nombre 20", "Nombre 20", "Phone 20" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Managed_ManagedId",
                schema: "Managed",
                table: "Managed",
                column: "ManagedId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Managed",
                schema: "Managed");
        }
    }
}
