﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Manageds.Persistence.Database.Migrations
{
    public partial class migracion2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CellPhone",
                schema: "Managed",
                table: "Managed",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CellPhone",
                schema: "Managed",
                table: "Managed");
        }
    }
}
