﻿using Manageds.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace Manageds.Persistence.Database.Configuration
{
    public class ManagedConfiguration
    {        
        public ManagedConfiguration(EntityTypeBuilder<Managed> entityBuilder)
        {
            entityBuilder.HasIndex(x => x.ManagedId);
            entityBuilder.Property(x => x.Name).IsRequired().HasMaxLength(200);
            entityBuilder.Property(x => x.LastName).IsRequired().HasMaxLength(200);
            entityBuilder.Property(x => x.Phone).IsRequired().HasMaxLength(50);
            entityBuilder.Property(x => x.CellPhone).HasMaxLength(100);

            var manageds = new List<Managed>();

            for (int i = 1; i <= 20; i++)
            {
                manageds.Add(new Managed()
                {
                    ManagedId = i,
                    Name = $"Nombre {i}",
                    LastName = $"Nombre {i}",
                    Phone = $"Phone {i}"
                });
            }

            entityBuilder.HasData(manageds);
        }
    }
}
