﻿namespace Resolutions.Domain
{
    public class ResolutionActa
    {
        public int ActaId { get; set; }
        public int ResolutionId { get; set; }
        public string Description { get; set; }
    }
}
