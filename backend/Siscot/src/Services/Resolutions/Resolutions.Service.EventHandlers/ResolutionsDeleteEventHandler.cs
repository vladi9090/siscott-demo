﻿using MediatR;
using Resolutions.Persistence.Database;
using Resolutions.Service.EventHandlers.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Resolutions.Service.EventHandlers
{
    public class ResolutionsDeleteEventHandler : INotificationHandler<ResolutionDeleteCommand>
    {
        private readonly ApplicationDbContext _context;

        public ResolutionsDeleteEventHandler(
             ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ResolutionDeleteCommand command, CancellationToken cancellationToken)
        {
            var resolution = _context.Resolution.Single(x => x.ResolutionId == command.ResolutionId);

            _context.Remove(resolution);

            await _context.SaveChangesAsync();
        }
    }
}
