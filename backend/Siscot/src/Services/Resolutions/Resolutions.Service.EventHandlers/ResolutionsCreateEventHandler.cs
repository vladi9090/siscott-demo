﻿using MediatR;
using Resolutions.Domain;
using Resolutions.Persistence.Database;
using Resolutions.Service.EventHandlers.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace Resolutions.Service.EventHandlers
{

    public class ResolutionsCreateEventHandler
        : INotificationHandler<ResolutionCreateCommand>
    {

        private readonly ApplicationDbContext _context;

        public ResolutionsCreateEventHandler(
             ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ResolutionCreateCommand command, CancellationToken cancellationToken)
        {
            await _context.AddAsync(new Resolution()
            { 
                
                Description = command.Description
            });

            await _context.SaveChangesAsync();
        }
    }
}
