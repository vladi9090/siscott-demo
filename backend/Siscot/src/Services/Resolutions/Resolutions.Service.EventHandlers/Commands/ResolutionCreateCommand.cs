﻿using MediatR;

namespace Resolutions.Service.EventHandlers.Commands
{
    public class ResolutionCreateCommand : INotification
    {
        public int ResolutionId { get; set; }
        public string Description { get; set; }
    }
}
