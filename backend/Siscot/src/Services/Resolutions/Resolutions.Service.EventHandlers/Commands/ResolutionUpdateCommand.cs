﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resolutions.Service.EventHandlers.Commands
{
    public class ResolutionUpdateCommand : INotification
    {
        public int ResolutionId { get; set; }
        public string Description { get; set; }
    }
}
