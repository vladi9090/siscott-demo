﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resolutions.Service.EventHandlers.Commands
{
    public class ResolutionDeleteCommand : INotification
    {
        public int ResolutionId { get; set; }
    }
}
