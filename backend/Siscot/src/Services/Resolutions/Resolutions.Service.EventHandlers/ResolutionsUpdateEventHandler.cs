﻿using MediatR;
using Resolutions.Persistence.Database;
using Resolutions.Service.EventHandlers.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Resolutions.Service.EventHandlers
{
    public class ResolutionsUpdateEventHandler : INotificationHandler<ResolutionUpdateCommand>
    {
        private readonly ApplicationDbContext _context;
        public ResolutionsUpdateEventHandler(
             ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(ResolutionUpdateCommand command, CancellationToken cancellationToken)
        {
            var resolution = _context.Resolution.Single(x => x.ResolutionId == command.ResolutionId);

            resolution.Description = command.Description;

            await _context.SaveChangesAsync();
        }
    }
}
