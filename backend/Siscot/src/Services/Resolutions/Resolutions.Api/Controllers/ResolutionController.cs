﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Resolutions.Service.EventHandlers.Commands;
using Resolutions.Service.Queries;
using Resolutions.Service.Queries.DTOs;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Resolutions.Api.Controllers
{
    [ApiController]
    [Route("resolutions")]
    public class ResolutionController : ControllerBase
    {
        private readonly IResolutionQueryService _resolutionQueryService;
        private readonly ILogger<ResolutionController> _logger;
        private readonly IMediator _mediator;
       

        public ResolutionController(
            ILogger<ResolutionController> logger,
            IResolutionQueryService resolutionQueryService,
            IMediator mediator)
        {
            _logger = logger;
            _resolutionQueryService = resolutionQueryService;
            _mediator = mediator;
        }

        //resolutions
        [HttpGet]
        public async Task<DataCollection<ResolutionDto>> GetAll(int page =1, int take = 10, string ids = null)
        {
            IEnumerable<int> resolutions = null;
            if (!string.IsNullOrEmpty(ids))
            {
                resolutions = ids.Split(',').Select(x => Convert.ToInt32(x));
            }

            return await _resolutionQueryService.GetAllAsync(page, take, resolutions);
        }

        //resolutions/1
        [HttpGet("{id}")]
        public async Task<ResolutionDto> GetAll(int id)
        {
            return await _resolutionQueryService.GetAsync(id);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ResolutionCreateCommand command)
        {
            await _mediator.Publish(command);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update(ResolutionUpdateCommand command)
        {
            await _mediator.Publish(command);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(ResolutionDeleteCommand command)
        {
            await _mediator.Publish(command);
            return Ok();
        }
    }
}
