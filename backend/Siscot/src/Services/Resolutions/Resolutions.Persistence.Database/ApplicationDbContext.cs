﻿using Resolutions.Domain;
using Microsoft.EntityFrameworkCore;
using Resolutions.Persistence.Database.Configuration;

namespace Resolutions.Persistence.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options
            ) : base(options)
        {
            
        }

        public DbSet<Resolution> Resolution { get; set; }
        public DbSet<ResolutionActa> ResolutionActa { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //esquema de la bd
            builder.HasDefaultSchema("dbo");
            ModelConfig(builder);
        }

        public void ModelConfig(ModelBuilder modelBuilder)
        {
            new ResolutionConfiguration(modelBuilder.Entity<Resolution>());
            new ResolutionActaConfiguration(modelBuilder.Entity<ResolutionActa>());
        }
    }
}
