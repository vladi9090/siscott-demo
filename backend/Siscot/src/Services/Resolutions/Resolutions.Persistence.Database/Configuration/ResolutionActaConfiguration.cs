﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Resolutions.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resolutions.Persistence.Database.Configuration
{
    public class ResolutionActaConfiguration
    {
        public ResolutionActaConfiguration(EntityTypeBuilder<ResolutionActa> entityBuilder)
        {
            entityBuilder.HasKey(x => x.ActaId);

            var resolutions = new List<ResolutionActa>();

            for (int i = 1; i <= 20; i++)
            {
                resolutions.Add(new ResolutionActa()
                {
                    ActaId = i,
                    ResolutionId = i,
                    Description = $"Descripcion de la resolucion {i}",
                });
            }

            entityBuilder.HasData(resolutions);
        }
    }
}
