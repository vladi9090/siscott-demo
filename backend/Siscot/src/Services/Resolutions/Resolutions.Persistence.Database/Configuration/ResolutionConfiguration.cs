﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Resolutions.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Resolutions.Persistence.Database.Configuration
{
    public class ResolutionConfiguration
    {
        public ResolutionConfiguration(EntityTypeBuilder<Resolution> entityBuilder)
        {
            entityBuilder.HasIndex(x => x.ResolutionId);
            entityBuilder.Property(x => x.Description).IsRequired().HasMaxLength(500);

            var resolutions = new List<Resolution>();

            for (int i = 1; i <= 20; i++)
            {
                resolutions.Add(new Resolution()
                {
                    ResolutionId = i,
                    Description = $"Descripcion de la resolucion {i}",
                });
            }

            entityBuilder.HasData(resolutions);
        }
    }
}
