﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Resolutions.Persistence.Database.Migrations
{
    public partial class initialize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Resolution");

            migrationBuilder.CreateTable(
                name: "Resolution",
                schema: "Resolution",
                columns: table => new
                {
                    ResolutionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resolution", x => x.ResolutionId);
                });

            migrationBuilder.CreateTable(
                name: "ResolutionActa",
                schema: "Resolution",
                columns: table => new
                {
                    ActaId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ResolutionId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResolutionActa", x => x.ActaId);
                    table.ForeignKey(
                        name: "FK_ResolutionActa_Resolution_ResolutionId",
                        column: x => x.ResolutionId,
                        principalSchema: "Resolution",
                        principalTable: "Resolution",
                        principalColumn: "ResolutionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "Resolution",
                table: "Resolution",
                columns: new[] { "ResolutionId", "Description" },
                values: new object[,]
                {
                    { 1, "Descripcion de la resolucion 1" },
                    { 18, "Descripcion de la resolucion 18" },
                    { 17, "Descripcion de la resolucion 17" },
                    { 16, "Descripcion de la resolucion 16" },
                    { 15, "Descripcion de la resolucion 15" },
                    { 14, "Descripcion de la resolucion 14" },
                    { 13, "Descripcion de la resolucion 13" },
                    { 12, "Descripcion de la resolucion 12" },
                    { 11, "Descripcion de la resolucion 11" },
                    { 10, "Descripcion de la resolucion 10" },
                    { 9, "Descripcion de la resolucion 9" },
                    { 8, "Descripcion de la resolucion 8" },
                    { 7, "Descripcion de la resolucion 7" },
                    { 6, "Descripcion de la resolucion 6" },
                    { 5, "Descripcion de la resolucion 5" },
                    { 4, "Descripcion de la resolucion 4" },
                    { 3, "Descripcion de la resolucion 3" },
                    { 2, "Descripcion de la resolucion 2" },
                    { 19, "Descripcion de la resolucion 19" },
                    { 20, "Descripcion de la resolucion 20" }
                });

            migrationBuilder.InsertData(
                schema: "Resolution",
                table: "ResolutionActa",
                columns: new[] { "ActaId", "Description", "ResolutionId" },
                values: new object[,]
                {
                    { 1, "Descripcion de la resolucion 1", 1 },
                    { 18, "Descripcion de la resolucion 18", 18 },
                    { 17, "Descripcion de la resolucion 17", 17 },
                    { 16, "Descripcion de la resolucion 16", 16 },
                    { 15, "Descripcion de la resolucion 15", 15 },
                    { 14, "Descripcion de la resolucion 14", 14 },
                    { 13, "Descripcion de la resolucion 13", 13 },
                    { 12, "Descripcion de la resolucion 12", 12 },
                    { 11, "Descripcion de la resolucion 11", 11 },
                    { 10, "Descripcion de la resolucion 10", 10 },
                    { 9, "Descripcion de la resolucion 9", 9 },
                    { 8, "Descripcion de la resolucion 8", 8 },
                    { 7, "Descripcion de la resolucion 7", 7 },
                    { 6, "Descripcion de la resolucion 6", 6 },
                    { 5, "Descripcion de la resolucion 5", 5 },
                    { 4, "Descripcion de la resolucion 4", 4 },
                    { 3, "Descripcion de la resolucion 3", 3 },
                    { 2, "Descripcion de la resolucion 2", 2 },
                    { 19, "Descripcion de la resolucion 19", 19 },
                    { 20, "Descripcion de la resolucion 20", 20 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Resolution_ResolutionId",
                schema: "Resolution",
                table: "Resolution",
                column: "ResolutionId");

            migrationBuilder.CreateIndex(
                name: "IX_ResolutionActa_ResolutionId",
                schema: "Resolution",
                table: "ResolutionActa",
                column: "ResolutionId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ResolutionActa",
                schema: "Resolution");

            migrationBuilder.DropTable(
                name: "Resolution",
                schema: "Resolution");
        }
    }
}
