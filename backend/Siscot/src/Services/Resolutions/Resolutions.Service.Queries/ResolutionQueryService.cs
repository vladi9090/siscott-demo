﻿using Microsoft.EntityFrameworkCore;
using Resolutions.Persistence.Database;
using Resolutions.Service.Queries.DTOs;
using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Resolutions.Service.Queries
{
    public interface IResolutionQueryService
    {
        Task<DataCollection<ResolutionDto>> GetAllAsync(int page, int take, IEnumerable<int> resolutions = null);
        Task<ResolutionDto> GetAsync(int Id);
    }

    public class ResolutionQueryService : IResolutionQueryService
    {
        private readonly ApplicationDbContext _context;
        public ResolutionQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DataCollection<ResolutionDto>> GetAllAsync(int page, int take, IEnumerable<int> resolutions = null)
        {
            //var collection = await _context.Resolution
            //    .Where(x => resolutions == null || resolutions.Contains(x.ResolutionId))
            //    .OrderByDescending(x => x.ResolutionId)
            //    .Union(_context.ResolutionActa.Select(a=>a.ResolutionId==x.))
            //    .GetPagedAsync(page, take);


            //var query =
            //    _context.Brand.Select(x => x.Brandid)
            //        .Union(_context.Factory.Select(x => x.Factorycode))
            //        .Union(_context.Brandfactory.Select(x => x.Factoryid));

            var collection = await _context.Resolution.Select(x => x.ResolutionId)
                .Union(_context.ResolutionActa.Select(a => a.ResolutionId))
                .Union(_context.Resolution.Select(x => x.ResolutionId))
                .GetPagedAsync(page, take);


            return collection.MapTo<DataCollection<ResolutionDto>>();
        }

        public async Task<ResolutionDto> GetAsync(int Id)
        {
            return (await _context.Resolution.SingleAsync(x => x.ResolutionId == Id)).MapTo<ResolutionDto>();
        }



    }
}
