﻿namespace Resolutions.Service.Queries.DTOs
{
    public class ResolutionActaDto
    {
        public int ActaId { get; set; }
        public int ResolutionId { get; set; }
        public string Description { get; set; }
    }
}
