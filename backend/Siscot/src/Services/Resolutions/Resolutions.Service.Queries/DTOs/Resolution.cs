﻿namespace Resolutions.Service.Queries.DTOs
{
    public class ResolutionDto
    {
        public int ResolutionId { get; set; }
        public string Description { get; set; }
       // public ResolutionActaDto Acta { get; set; }
    }
}
