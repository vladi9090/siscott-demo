﻿using Expediente.Persistence.Database;
using Expediente.Service.Queries.DTOs;

using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expediente.Service.Queries
{

    public interface IActaResolucionMasivaQueryService
    {
        Task<DataCollection<ActaResolucionMasivaDto>> GetActaResolucion(RequestActaResolucionDto request);

    }
    public class ActaResolucionMasivaQueryService : IActaResolucionMasivaQueryService
    {
        private readonly ApplicationDbContext _context;

        public ActaResolucionMasivaQueryService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<DataCollection<ActaResolucionMasivaDto>> GetActaResolucion(RequestActaResolucionDto request)
        {

            var collection = await (from acta in _context.Documento_Control

                                    join acta_detalle in _context.Documento_Control_Detalle on acta.id_documento_control equals acta_detalle.id_documento_control
                                    join ent_acta in _context.Entidad.Where(ent_ac => String.IsNullOrWhiteSpace(request.administrado) || ent_ac.entidad_nombre.Contains(request.administrado))
                                    on acta_detalle.id_entidad equals ent_acta.id_entidad
                                    join falta in _context.Falta.Where(falta_ac => request.tipo_falta == 0 || falta_ac.id_falta == request.tipo_falta) on acta_detalle.id_falta equals falta.id_falta

                                    join detalle_expe in _context.Expediente_Administrativo_Detalle on acta_detalle.id_documento_control equals detalle_expe.id_documento_control
                                    join expediente in _context.Expediente_Administrativo.Where(exp_ac => String.IsNullOrWhiteSpace(request.numero_expediente) || exp_ac.numero_expediente.Contains(request.numero_expediente)) on acta.id_documento_control equals expediente.id_expediente

                                    join resol_detalle in _context.Resolucion_Administrativa_Detalle.Where(r => r.estado != "X") on acta.id_documento_control equals resol_detalle.id_documento_control into into_resol_det
                                    from _resol_detalle in into_resol_det.DefaultIfEmpty()

                                    join resol in _context.Resolucion_Administrativa.Where(resol => request.tipo_resolucion == 0 || resol.id_tipo_resolucion == request.tipo_resolucion) on _resol_detalle.id_resolucion equals resol.id_resolucion into into_resol_ad
                                    from _resol in into_resol_ad.DefaultIfEmpty()

                                    join parametro in _context.Parametro on _resol.id_tipo_resolucion equals parametro.id_parametro into into_parametro
                                    from parametro in into_parametro.DefaultIfEmpty()

                                    join resol_ent in _context.Resolucion_Administrado.Where(re => re.estado != "X") on _resol.id_resolucion equals resol_ent.id_resolucion into into_resol_ent
                                    from _resol_ent in into_resol_ent.DefaultIfEmpty()

                                    join ent_resol in _context.Entidad on _resol_ent.id_entidad equals ent_resol.id_entidad into into_ent_resol
                                    from _ent_resol in into_ent_resol.DefaultIfEmpty()

                                    //join  doc_resol  in _context.Resolucion_Adjunto on _resol.id_resolucion  equals doc_resol.id_resolucion into into_doc_resol from _doc_resol in into_doc_resol.DefaultIfEmpty()

                                    select new ActaResolucionMasivaDto
                                    {
                                        id_resolucion = _resol_detalle == null ? 0 : _resol_detalle.id_resolucion,
                                        descripcion_tipo_resolucion = _resol_detalle == null ? "" : parametro.valor,
                                        numero_resolucion = _resol_detalle == null ? "" : _resol.numero_resolucion,
                                        fecha_resolucion = _resol_detalle == null ? DateTime.MinValue : _resol.fecha_reg,
                                        usu_reg_resolucion = _resol_detalle == null ? "" : _resol.id_usuario_reg,
                                        administrado_resolucion = _resol_detalle == null ? "" : _ent_resol.entidad_nombre,
                                        id_documento_control = acta.id_documento_control,
                                        numero_acta = acta.numero_acta,
                                        fecha_acta = acta.fecha_reg,
                                        administrado_acta = ent_acta.entidad_nombre,
                                        id_entidad = ent_acta.id_entidad,
                                        secuencia = acta_detalle.secuencia,
                                        id_falta = falta.id_falta,
                                        codigo_falta = falta.clave,
                                        numero_expediente = expediente.numero_expediente,
                                        //id_documento_externo = _doc_resol==null ? 0 : _doc_resol.id_documento_externo
                                    })
                              .GetPagedAsync(request.page, request.take);

            return collection.MapTo<DataCollection<ActaResolucionMasivaDto>>();
        }
    }
}
