﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class TipoFormatoDto
    {
        public int id_tipo_formato { get; set; }
        public string nombre_formato { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }
        public int id_grupo_formato { get; set; }
    }
}
