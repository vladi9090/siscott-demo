﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class RequestActaResolucionDto : PageDto
    {
        public int grupo_formato { get; set; }
        public int tipo_resolucion { get; set; }
        public int tipo_falta { get; set; }
        public int grupo_resolutor { get; set; }
        public string numero_actas { get; set; }
        public string administrado { get; set; }
        public string numero_expediente { get; set; }
        public int id_resolucion { get; set; }

    }
}
