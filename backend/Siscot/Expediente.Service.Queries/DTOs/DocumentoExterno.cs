﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class DocumentoExternoDto
    {
        public int id_documento_externo { get; set; }
        public int id_tipo_documento { get; set; }
        public string sumilla { get; set; }
        public string extension { get; set; }
        public string nombre_archivo { get; set; }
        public string ruta_archivo { get; set; }
        public string tipo_contenido { get; set; }
        public string nombre_fisico { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public string estado { get; set; }
    }
}
