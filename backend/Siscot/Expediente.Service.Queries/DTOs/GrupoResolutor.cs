﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class GrupoResolutorDto
    {
        public int id_grupo_resolutor { get; set; }
        public string descripcion { get; set; }
        public string estado { get; set; }
    }
}
