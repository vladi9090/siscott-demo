﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class ActaResolucionMasivaDto
    {
        public int id_documento_control { get; set; }

        public string numero_acta { get; set; }

        public DateTime? fecha_acta { get; set; }

        public string administrado_acta { get; set; }

        public int id_falta { get; set; }

        public string codigo_falta { get; set; }

        public Int64 id_resolucion { get; set; }

        public string descripcion_tipo_resolucion { get; set; }

        public string numero_resolucion { get; set; }

        public string numero_expediente { get; set; }

        public string administrado_resolucion { get; set; }

        public DateTime? fecha_resolucion { get; set; }

        public string usu_reg_resolucion { get; set; }

        public string estado_pago { get; set; }
        public int id_notificacion { get; set; }
        public string notificacion { get; set; }

        public int id_tipo_resolucion { get; set; }

        public int secuencia { get; set; }

        public int id_entidad { get; set; }
        //public string estado { get; set; }
        public string tipo_resolucion_plantilla { get; set; }

        public int id_documento_externo { get; set; }
    }
}
