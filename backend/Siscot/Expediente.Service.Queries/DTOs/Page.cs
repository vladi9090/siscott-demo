﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class PageDto
    {
        public int page { get; set; }

        public int take { get; set; }

    }
}
