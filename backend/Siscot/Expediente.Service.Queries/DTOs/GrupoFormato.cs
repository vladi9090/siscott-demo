﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expediente.Service.Queries.DTOs
{
    public class GrupoFormatoDto
    {
        public int id_grupo_formato { get; set; }
        public string nombre_grupo_formato { get; set; }
        public string estado { get; set; }
    }
}
