﻿
using Expediente.Domain;
using Expediente.Persistence.Database;
using Expediente.Service.Queries.DTOs;
using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Expediente.Service.Queries
{
    public interface IDependenciaQueryService
    {
        Task<DataCollection<DependenciaDto>> ListarDependencia(int page, int take);
    }
    public class DependenciaQueryService : IDependenciaQueryService
    {
        
        private readonly ApplicationDbContext _context;

        public DependenciaQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DataCollection<DependenciaDto>> ListarDependencia(int page, int take)
        { 
            var collection = await _context.Dependencia
                .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<DependenciaDto>>();
        }
    }
}
