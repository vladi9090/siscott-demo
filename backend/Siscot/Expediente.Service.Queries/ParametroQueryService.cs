﻿using Expediente.Persistence.Database;
using Expediente.Service.Queries.DTOs;
using Service.Common.Collection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Expediente.Domain;
using Service.Common.Mapping;
using Service.Common.Paging;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Expediente.Service.Queries
{

    public interface IParametroQueryService
    {
        Task<DataCollection<ParametroDto>> GetTipoPlantillaResolucion(int IdTipoResolucion, int page, int take);
        Task<DataCollection<ParametroGrupoDto>> GetListParametroGrupo(int page, int take);
        Task<DataCollection<ParametroDto>> GetListarTipoResolucion(int page, int take);
        Task<DataCollection<GrupoResolutorDto>> GetListarGrupoResolutor(int page, int take);
        Task<DataCollection<ParametroDto>> GetListarTipoServicio(int page, int take);
        Task<DataCollection<TipoFormatoDto>> GetListTipoFormato(int page, int take);
        Task<DataCollection<GrupoFormatoDto>> GetListGrupoFormato(int page, int take);
        Task<DataCollection<TipoFormatoDto>> GetListTipoFormatoXGrupo(int page, int take, int id);
        Task<DataCollection<ParametroDto>> GetListTipoServicioXFormato(int page, int take, int id);
        Task<ParametroDto>GetListTipoPlantilla(int id);

    }
    public class ParametroQueryService : IParametroQueryService
    {
        private readonly ApplicationDbContext _context;
        public ParametroQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DataCollection<ParametroDto>> GetTipoPlantillaResolucion(int IdTipoResolucion,int page, int take)
        {
            var collection = await (from p in _context.Parametro
                                    join pg in _context.Parametro_Grupo on p.id_grupo_parametro equals pg.id_grupo_parametro
                                    where pg.nombre_grupo == "Tipo Plantilla Resolucion" && p.id_padre==IdTipoResolucion
                                    select new
                                    {
                                        id_parametro = p.id_parametro,
                                        valor = p.valor
                                    })
                              .GetPagedAsync(page, take);
            return collection.MapTo<DataCollection<ParametroDto>>();

        }
        public async Task<DataCollection<ParametroGrupoDto>> GetListParametroGrupo(int page, int take)
        {
            var collection = await (from pg in _context.Parametro_Grupo
                                    select new
                                     {
                                         id_grupo_parametro = pg.id_grupo_parametro,
                                         nombre_grupo = pg.nombre_grupo    
                                     })
                                    .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<ParametroGrupoDto>>();

        }

        public async Task<DataCollection<ParametroDto>> GetListarTipoResolucion(int page, int take)
        {
            var collection = await (from p in _context.Parametro
                                    join pg in _context.Parametro_Grupo on p.id_grupo_parametro equals pg.id_grupo_parametro
                                    where pg.nombre_grupo == "Tipo Resoluciones"
                                    select new
                                    {
                                        id_parametro = p.id_parametro,
                                        valor = p.valor
                                    })
                              .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<ParametroDto>>();
        }

        public async Task<DataCollection<GrupoResolutorDto>> GetListarGrupoResolutor(int page, int take)
        {
            var collection = await (from pg in _context.Grupo_Resolutor
                                    select new
                                    {
                                        id_grupo_resolutor = pg.id_grupo_resolutor,
                                        descripcion = pg.descripcion
                                    })
                                    .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<GrupoResolutorDto>>();

        }

        public async Task<DataCollection<ParametroDto>> GetListarTipoServicio(int page, int take)
        {
            var collection = await (from p in _context.Parametro
                                    join pg in _context.Parametro_Grupo on p.id_grupo_parametro equals pg.id_grupo_parametro
                                    where pg.nombre_grupo == "Tipo Servicio"
                                    select new
                                    {
                                        id_parametro = p.id_parametro,
                                        valor = p.valor
                                    })
                              .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<ParametroDto>>();
        }

        public async Task<DataCollection<TipoFormatoDto>> GetListTipoFormato(int page, int take)
        {
            var collection = await (from tf in _context.Tipo_Formato
                                    select new
                                    {
                                        id_tipo_formato = tf.id_tipo_formato,
                                        nombre_formato = tf.nombre_formato
                                    })
                                    .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<TipoFormatoDto>>();
        }

        public async Task<DataCollection<GrupoFormatoDto>> GetListGrupoFormato(int page, int take)
        {
            var collection = await (from gf in _context.Grupo_Formato
                                    select new
                                    {
                                        id_grupo_formato = gf.id_grupo_formato,
                                        nombre_grupo_formato = gf.nombre_grupo_formato
                                    })
                                    .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<GrupoFormatoDto>>();
        }

        public async Task<DataCollection<TipoFormatoDto>> GetListTipoFormatoXGrupo(int page, int take, int id)
        {
            var collection = await (from tf in _context.Tipo_Formato
                                    where tf.id_grupo_formato == id 
                                    select new
                                    {
                                        id_tipo_formato = tf.id_tipo_formato,
                                        nombre_formato = tf.nombre_formato
                                    })
                                    .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<TipoFormatoDto>>();
        }

        public async Task<DataCollection<ParametroDto>> GetListTipoServicioXFormato(int page, int take, int id)
        {
            var collection = await (from p in _context.Parametro
                                    join pg in _context.Parametro_Grupo on p.id_grupo_parametro equals pg.id_grupo_parametro
                                    where pg.nombre_grupo == "Tipo Servicio" && p.id_tipo_formato == id
                                    select new
                                    {
                                        id_parametro = p.id_parametro,
                                        valor = p.valor
                                    })
                              .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<ParametroDto>>();
        }

        public async Task<ParametroDto> GetListTipoPlantilla(int id)
        {
            var collection = await (from p in _context.Parametro
                                    join pg in _context.Parametro_Grupo on p.id_grupo_parametro equals pg.id_grupo_parametro
                                    where p.id_parametro == id
                                    select new
                                    {
                                        id_parametro = p.id_parametro,
                                        valor = p.valor,
                                        sigla = p.sigla
                                    })
                              .SingleOrDefaultAsync();

            return collection.MapTo<ParametroDto>();
        }

    }
}
