﻿using Expediente.Persistence.Database;
using Expediente.Service.EventHandlers.Commands;
using Expediente.Service.Queries.DTOs;
using Microsoft.EntityFrameworkCore;
using Service.Common.Collection;
using Service.Common.Mapping;
using Service.Common.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expediente.Service.Queries
{

    public interface IResolucionQueryService
    {

        Task<DataCollection<ResolucionAdministrativaDto>> GetListarResolucion(int page, int take);
        Task<ActaResolucionMasivaDto> GetDatosResolucion(ActaResolucionMasivaDto request);

    }
    public class ResolucionQueryService : IResolucionQueryService
    {

        private readonly ApplicationDbContext _context;

        public ResolucionQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DataCollection<ResolucionAdministrativaDto>> GetListarResolucion(int page, int take)
        {
            var collection = await (from ra in _context.Resolucion_Administrativa
                                    join par in _context.Parametro on ra.id_tipo_resolucion equals par.id_parametro
                                    select new
                                    {
                                        id_resolucion = ra.id_resolucion,
                                        descripcion_tipo_resolucion = par.valor,
                                        numero_resolucion = ra.numero_resolucion,
                                        fecha_reg = ra.fecha_reg,
                                        id_usuario_reg = ra.id_usuario_reg,
                                        estado = ra.estado,
                                        id_usuario_mod = ra.id_usuario_mod
                                    })
                              .GetPagedAsync(page, take);

            return collection.MapTo<DataCollection<ResolucionAdministrativaDto>>();
        }

        public async Task<ActaResolucionMasivaDto> GetDatosResolucion(ActaResolucionMasivaDto request)
        {
            var collection = await (from resol in _context.Resolucion_Administrativa
                                    join resol_detalle in _context.Resolucion_Administrativa_Detalle on resol.id_resolucion equals resol_detalle.id_resolucion
                                    join resol_ent in _context.Resolucion_Administrado on resol.id_resolucion equals resol_ent.id_resolucion
                                    join acta in _context.Documento_Control on resol_detalle.id_documento_control equals acta.id_documento_control
                                    join acta_detalle in _context.Documento_Control_Detalle on acta.id_documento_control equals acta_detalle.id_documento_control
                                    join ent_resol in _context.Entidad on resol_ent.id_entidad equals ent_resol.id_entidad
                                    join falta in _context.Falta on resol_detalle.id_falta equals falta.id_falta
                                    join detalle_expe in _context.Expediente_Administrativo_Detalle on acta_detalle.id_documento_control equals detalle_expe.id_documento_control
                                    join expediente in _context.Expediente_Administrativo on detalle_expe.id_expediente equals expediente.id_expediente
                                    join parametro in _context.Parametro on resol.id_tipo_resolucion equals parametro.id_parametro
                                    //join tipoplantilla in _context.Parametro on resol.id_tipo_plantilla equals tipoplantilla.id_parametro
                                    where resol.id_tipo_resolucion == request.id_tipo_resolucion && resol.id_resolucion == request.id_resolucion
                                    select new ActaResolucionMasivaDto
                                    {
                                        id_resolucion = resol.id_resolucion,
                                        id_tipo_resolucion = resol.id_tipo_resolucion,
                                        descripcion_tipo_resolucion = parametro.valor,
                                        numero_resolucion = resol.numero_resolucion,
                                        fecha_resolucion = resol.fecha_reg,
                                        usu_reg_resolucion = resol.id_usuario_reg,
                                        administrado_resolucion = ent_resol.entidad_nombre,
                                        id_documento_control = acta.id_documento_control,
                                        numero_acta = acta.numero_acta,
                                        fecha_acta = acta.fecha_reg,
                                        administrado_acta = ent_resol.entidad_nombre,
                                        id_falta = falta.id_falta,
                                        codigo_falta = falta.clave,
                                        numero_expediente = expediente.numero_expediente,


                                        //tipo_resolucion_plantilla = tipoplantilla.sigla
                                    })
                                    .SingleOrDefaultAsync();

            return collection.MapTo<ActaResolucionMasivaDto>();

        }

    }
}
