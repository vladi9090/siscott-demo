﻿using Expediente.Domain;
using Expediente.Persistence.Database;
using Expediente.Service.Queries.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Service.Common.Mapping;

namespace Expediente.Service.Queries
{

    public interface IDocumentoExternoQueryService
    {
        Task<DocumentoExternoDto> GetDocumentoExternoxId(int id);
    }

    public class DocumentoExternoQueryService : IDocumentoExternoQueryService
    {
        private readonly ApplicationDbContext _context;
        public DocumentoExternoQueryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DocumentoExternoDto> GetDocumentoExternoxId(int id)
        {
            var collection = await (from de in _context.Documento_Externo
                                    where de.id_documento_externo == id
                                    select new DocumentoExternoDto
                                    {
                                        id_documento_externo = de.id_documento_externo,
                                        nombre_archivo = de.nombre_archivo,
                                        ruta_archivo = de.ruta_archivo,
                                        nombre_fisico = de.nombre_fisico
                                    })
                                    .SingleOrDefaultAsync();

            return collection.MapTo<DocumentoExternoDto>();
        }
    }
}
