﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Parametro
    {
        [Key]
        public int id_parametro{ get; set; }
        public int id_grupo_parametro { get; set; }
        public string valor { get; set; }
        public int orden { get; set; }
        public int id_tipo_formato { get; set; }
        public string sigla { get; set; }
        public int id_padre { get; set; }


    }


}
