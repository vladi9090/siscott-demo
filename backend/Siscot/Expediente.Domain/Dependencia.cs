﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Dependencia
    {
        [Key]
        public int id_dependencia { get; set; }
        public int id_unidad_desconcentrada { get; set; }
        public int id_oficina_unidad_desconcentrada { get; set; }
        public string codigo { get; set;}
        public string sufijo { get; set; }
        public string estado { get; set; }

    }
}
