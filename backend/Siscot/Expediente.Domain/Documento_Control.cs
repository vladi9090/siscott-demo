﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Documento_Control
    {

        [Key]
        public int id_documento_control { get; set; }
        public int id_tipo_formato { get; set; }
        public string numero_acta { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }
    }
}
