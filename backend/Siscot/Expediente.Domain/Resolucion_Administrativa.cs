﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Expediente.Domain
{
    public class Resolucion_Administrativa
    {

        [Key]
        public int id_resolucion { get; set; }
        public string numero_resolucion { get; set; }
        public int id_grupo_resolutor { get; set; }
        public int id_dependencia { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }
        public int id_tipo_resolucion { get; set; }
        public int id_tipo_plantilla { get; set; }


    }
}
