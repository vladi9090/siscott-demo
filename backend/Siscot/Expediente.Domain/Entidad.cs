﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Entidad
    {
        [Key]
        public int id_entidad { get; set; }
        public int id_tipo_documento_entidad { get; set; }
        public string numero_documento { get; set; }
        public string entidad_nombre { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }

    }
}
