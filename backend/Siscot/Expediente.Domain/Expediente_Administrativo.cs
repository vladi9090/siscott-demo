﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Expediente_Administrativo
    {
        [Key]
        public int id_expediente { get; set; }
        public string numero_expediente { get; set; }
        public int id_documento_administrativo { get; set; }
        public string agrupado { get; set; }
        public string es_pasivo { get; set; }
        public string es_coactivo { get; set; }
        public string expediente_coactivo { get; set; }
        public string codigo_pago { get; set; }
        public double monto { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }

    }
}
