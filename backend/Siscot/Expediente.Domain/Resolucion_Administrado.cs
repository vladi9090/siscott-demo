﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Resolucion_Administrado
    {
        [Key]
        public int  id_administrado_resolucion { get; set; }
        public int id_resolucion { get; set; }
        public int id_entidad { get; set; }
        public int? id_entidad_direccion { get; set; }
        public string notificado { get; set; }
        public string casilla { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }
    }
}
