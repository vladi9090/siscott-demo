﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Resolucion_Adjunto
    {
        [Key]
        public int id_resolucion_adjunto { get; set; }
        public Int64 id_resolucion { get; set; }
        public int id_documento_externo { get; set; }
        public string estado { get; set; }


    }
}
