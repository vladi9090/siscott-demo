﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Grupo_Formato
    {
        [Key]
        public int id_grupo_formato { get; set; }
        public string nombre_grupo_formato { get; set; }
        public string estado { get; set; }
    }
}
