﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public class Grupo_Resolutor
    {
        [Key]
        public int id_grupo_resolutor { get; set; }
        public string descripcion { get; set; }
        public string estado { get; set; }
    }
}
