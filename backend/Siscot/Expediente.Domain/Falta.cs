﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expediente.Domain
{
    public  class Falta
    {
        [Key]
        public int id_falta { get; set; }
        public string nombre_falta { get; set; }
        public string clave { get; set; }
        public DateTime fecha_reg { get; set; }
        public string id_usuario_reg { get; set; }
        public DateTime fecha_mod { get; set; }
        public string id_usuario_mod { get; set; }
        public string estado { get; set; }

    }
}
